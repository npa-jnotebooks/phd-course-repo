#!/bin/ksh

if(($#!=2));then
echo;echo Usage: $0 OUTPUT_DIR PLOT_NAME; echo; exit
fi


OUTDIR=$1
LABEL=$2

PLOT_NAME="${LABEL}.McMC.maps"


#
# GRID POINT FOR PLOTTING PURPOUSE
#
I_TRIANGULATE="0.002/0.002"
I_GRDSAMPLE="0.0005/0.0005"
#
#
#


gmt gmtset MEASURE_UNIT cm
gmt gmtset COLOR_NAN 175/255/130
gmt gmtset HEADER_FONT_SIZE 16
gmt gmtset ANOT_FONT 1
gmt gmtset ANOT_FONT_SIZE 8 
gmt gmtset GRID_PEN 0.01p
gmt gmtset TICK_LENGTH -0.2c
gmt gmtset BASEMAP_TYPE Plain
gmt gmtset PAPER_MEDIA a4
gmt gmtset PAGE_ORIENTATION Portrait 


PLOT_DIM_X='7'
PLOT_DIM_Y=$PLOT_DIM_X
PLOT_DIM=$PLOT_DIM_X/$PLOT_DIM_Y

LON_MIN="12.6"
LON_MAX="13.8"
LAT_MIN="42.2"
LAT_MAX="43.42"

RANGE="$LON_MIN/$LON_MAX/$LAT_MIN/$LAT_MAX"
TICKS="a0.5f0.1/a0.5f0.1"



#
# MEAN MOHO MAP
#


gmt makecpt -Cpolar -D -I -T10/80/5 > palette-moho.cpt

rm -f tmp.ps

gmt psbasemap -JM$PLOT_DIM -R$RANGE -B$TICKS -K -V -P -X2.5 -Y16 > tmp.ps

rm -f xyz.tmp
awk '{if(FNR>1){print $1,$2,$3 } }' mean001.001.out >> xyz.tmp

gmt triangulate xyz.tmp -R -Gxyz.0.grd -I$I_TRIANGULATE -V
gmt grdsample xyz.0.grd -Gxyz.grd -I$I_GRDSAMPLE -V 
gmt grdimage xyz.grd -J -Cpalette-moho.cpt -K -O -V >> tmp.ps


gmt pscoast -B$TICKS -Dh -R -J${PROJ} -W0.1 -S150/200/255 -L-6.5/51.5/-8/55/100 -V -O -K >> tmp.ps
rm -f stat.tmp
awk '{print $4, $5  }' obs.data >> stat.tmp
gmt psxy stat.tmp -J -R -St0.2 -G0 -O -K -V >> tmp.ps




dx=$(echo $PLOT_DIM_X | awk '{print $1/2}' - )
dy='10.5'
gmt pstext -J -R -O -D$dx/$dy -V -N -K << EOF >> tmp.ps
$LON_MIN $LAT_MIN 14 0 1 CM McMC: Mean PPD Moho Depth in Amatrice
EOF



#
# ERRORS on MOHO DEPTH MAP
#



gmt makecpt -Cgray -D -I -T0/30/2 > palette-std.cpt


gmt psbasemap -JM$PLOT_DIM -R$RANGE -B$TICKS -K -V -P -O -X10 -Y0 >> tmp.ps

rm -f xyz.tmp
awk '{if(FNR>1){print $1,$2,$4 } }' mean001.001.out >> xyz.tmp

gmt triangulate xyz.tmp -R -Gxyz.0.grd -I$I_TRIANGULATE -V > /dev/null
gmt grdsample xyz.0.grd -Gxyz.grd -I$I_GRDSAMPLE -V > /dev/null
gmt grdimage xyz.grd -J -Cpalette-std.cpt -K -O -V >> tmp.ps


gmt pscoast -B$TICKS -Dh -R -J -W0.1 -S150/200/255 -L-6.5/51.5/-8/55/100 -V -O -K >> tmp.ps
rm -f stat.tmp
awk '{print $4, $5 }' obs.data >> stat.tmp
gmt psxy stat.tmp -J -R -St0.2 -G0 -O -K -V >> tmp.ps




dx=$(echo $PLOT_DIM_X | awk '{print $1/2}' - )
dy='10.5'
pstext -J -R -O -D$dx/$dy -V -N -K  << EOF >> tmp.ps
$LON_MIN $LAT_MIN 14 0 1 CM McMC: 1-@~s@~ fom Moho depth
EOF



#
# NUCLEI DENSITY MAP
#


gmt makecpt -Ccopper -D -I -T0/100/10  > palette-dens.cpt

gmt psbasemap -JM$PLOT_DIM -R$RANGE -B$TICKS -K -V -P -O -X-10 -Y-12 >> tmp.ps

rm -f xyz.tmp
awk '{if(FNR>1){print $1,$2,$3 } }' mean001.002.out >> xyz.tmp

gmt triangulate xyz.tmp -R -Gxyz.0.grd -I$I_TRIANGULATE -V
gmt grdsample xyz.0.grd -Gxyz.grd -I$I_GRDSAMPLE -V
gmt grdimage xyz.grd -J -Cpalette-dens.cpt -K -O -V >> tmp.ps


gmt pscoast -B$TICKS -Dh -R -J${PROJ} -W0.1 -S150/200/255 -L-6.5/51.5/-8/55/100 -V -O -K >> tmp.ps
rm -f stat.tmp
awk '{print $4, $5 }' obs.data >> stat.tmp
gmt psxy stat.tmp -J -R -St0.2 -G0 -O -K -V >> tmp.ps




dx=$(echo $PLOT_DIM_X | awk '{print $1/2}' - )
dy='10.5'
gmt pstext -J -R -O -D$dx/$dy -V -K -N  << EOF >> tmp.ps
$LON_MIN $LAT_MIN 14 0 1 CM McMC: Mean nuclei density
EOF


dx="10"
dy="4"
pstext -J -R -O -D$dx/$dy -V -K -N  << EOF >> tmp.ps
$LON_MIN $LAT_MIN 10 90 1 CM  Moho depth (km)
EOF
psscale -D10.5/4/8/0.5 -Ba10f5 -Cpalette-moho.cpt -O -K -V >> tmp.ps

dx="12.5"
dy="4"
pstext -J -R -O -D$dx/$dy -V -K -N  << EOF >> tmp.ps
$LON_MIN $LAT_MIN 10 90 1 CM  1-@~s@~ (km)
EOF
psscale -D13/4/8/0.5 -Ba6f2 -Cpalette-std.cpt -O -K -V >> tmp.ps

dx="15"
dy="4"
pstext -J -R -O -D$dx/$dy -V -K -N  << EOF >> tmp.ps
$LON_MIN $LAT_MIN 10 90 1 CM  Nuclei density (km@+-2@+)
EOF
psscale -D15.5/4/8/0.5 -Ba20f5 -Cpalette-dens.cpt -O -V >> tmp.ps















rm -f *.tmp xyz.grd xyz.0.grd
ps2pdf tmp.ps ${PLOT_NAME}.pdf
rm -f tmp.ps
gv ${PLOT_NAME}.pdf



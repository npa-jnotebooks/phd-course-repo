c
c
c       subroutine GET_INITIAL_MODEL
c
c---------------------------------------------------------------------------------
c
c INPUT:
c         (none)
c
c OUTPUT:
c         n0 --   number of Voronoi nuclei in curent model
c         param0 -- parameter values for each Voronoi nucleus in curent model
c
c---------------------------------------------------------------------------------
c
c GET_INITIAL_MODEL:
c
c (1) N will be posed at a minimum
c (2) PARAM0 will be sampled randomly
c
c---------------------------------------------------------------------------------
c
c
        subroutine get_initial_model(n0,param0)


        include '../trans_dim.para'


        include '../modules/common.obs_syn'
        include '../modules/common.param'
        include '../modules/common.recipe'
        include '../modules/common.randomtype'

c
c MAIN variables
c
        integer n0
        real*4  param0(nc_max,nd_max)


c
c Scratch variables
c 
        integer ic, id

c
c RAN3 variables
c
        real*4 p, ran3


c
c Check for a imposed starting model
c

	open(10,file='start_model.dat',status='old',ERR=102)

        read(10,*)
        read(10,*)
        read(10,*)
        read(10,*) n0
        do ic=1,n0
          read(10,*) (param0(ic,id),id=1,nd)
        enddo

        close (10)

        goto 104


 102    continue

c
c
c RAMDOM STARTING MODEL
c
c
c  (1) N0=1


        n0=min_c
        

c
c  (2) PARAM0 from APRIORI_INFO

        do ic=1,n0
	  do id=1,nd
            p=ran3(iseed0)
            param0(ic,id)=apriori_info(id,1)+p*(apriori_info(id,2)-apriori_info(id,1))
	  enddo
        enddo

 104    continue



          write(*,*)
          write(*,'(a)') ' ++iRjSurfRec:: STARTING MODEL -- '
          do ic=1,n0
            write(*,'(a,i4,3f16.4)') ' ++RjSurfRec:: NUCLEUS: ',ic,(param0(ic,id), id=1,nd)
          enddo
          write(*,*)



        return
        end

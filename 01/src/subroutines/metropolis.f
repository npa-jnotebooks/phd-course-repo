c
c
c       subroutine METROPOLIS
c
c-------------------------------------------------------------------------------------------
c
c INPUT:
c         im --   model index
c         lppd -- log value of the norm factor of the Likelihood
c         lppd -- log value of the exponential component of the Likelihood
c
c OUTPUT:
c         accepted --   Flag for the Metropolis rule output (see below)
c
c-------------------------------------------------------------------------------------------
c
c METROPOLIS RULE OUTPUT will be:
c
c (ACCEPTED= -1) Candidate model is accepted w/ Metropolis rule (Likelihood is decreased)
c (ACCEPTED=  0) Candidate model is rejected
c (ACCEPTED=  1) Candidate model is accepted w/o Metropolis rule (Likelihood is increased)
c
c-------------------------------------------------------------------------------------------
c
c
        subroutine metropolis(im,lppd,accepted)


        include '../trans_dim.para'


        include '../modules/common.recipe'
        include '../modules/common.misfit'
        include '../modules/common.randomtype'

c
c MAIN variables

        integer im, accepted
        real*4 lppd


c
c Scratch variables

        real*4 alpha

c
c RAN3 variables
        real*4 p, ran3


        if(im.eq.1)alpha=1.0
        if(im.ne.1)       alpha=  1.d0 *
     &       exp( -0.5d0*(lppd-lppd0) )

        if(info)write(*,'(a,4f16.4)') ' ++RjSurfRec:: METROPOLIS lppd/lppd0 :',lppd,lppd0
        if(info)write(*,'(a,1f24.4)') ' ++RjSurfRec:: METROPOLIS -- ALPHA: ', alpha
        if(.not.posterior)alpha=1.0

        if(alpha.ge.1.0)then

          accepted=1
          lppd0=lppd
          if(info)write(*,'(a,i6)') ' ++RjSurfRec:: METROPOLIS -- Cand ACCEPTED w/o RULE: ', accepted

        else if(alpha.lt.1.0)then

          p=ran3(iseed0)

          if(alpha.ge.p)then
     
            accepted=-1
            lppd0=lppd
            if(info)write(*,'(a,f10.4,i6)') ' ++RjSurfRec:: METROPOLIS -- Cand ACCEPTED w/ RULE: ', alpha, accepted


          else

            accepted=0
            if(info)write(*,'(a,f10.4,i6)') ' ++RjSurfRec:: METROPOLIS -- Cand REJECTED: ', alpha, accepted


          endif

        endif



        return
        end

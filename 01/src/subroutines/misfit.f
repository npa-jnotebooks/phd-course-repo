c
c
c	subroutine MISFIT
c
c-------------------------------------------------------------------------
c
c INPUT:
c        hparam  --  scaling factor for the STD
c
c OUTPUT:
c         lppd -- logarithmic value of the PPD (i.e Chi-squared value)
c
c-------------------------------------------------------------------------
c
c
        subroutine misfit(lppd)

        

        include '../trans_dim.para'


        include '../modules/common.obs_syn'
        include '../modules/common.param'
        include '../modules/common.recipe'


c
c MAIN variables

        real*4  lppd



c
c Scratch variables

        integer id
        real*4  diff

       
c
c COMPUTE FIT AS A CHI-SQUARE VALUE. COVARIANCE ERROR MATRIX IS CONSIDERED DIAGONAL
c


        diff=0.e0

        do id=1,ndata

c Misfit over ACCELLERATION

            diff = diff + ((obs_D(id,3)-syn_D(id))/(obs_D(id,4)))**2
	    if(info) write(*,'(a,5f12.4)') '++Misfit:: ', obs_D(id,3), syn_D(id), obs_D(id,4), diff

        enddo


        lppd=diff

       

        return
        end

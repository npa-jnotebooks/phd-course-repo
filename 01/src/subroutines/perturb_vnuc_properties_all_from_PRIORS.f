

        subroutine perturb_all_vnuc_properties_from_priors(id_sele,n,param0,param)


        include '../trans_dim.para'


        include '../modules/common.obs_syn'
        include '../modules/common.param'
        include '../modules/common.recipe'
        include '../modules/common.randomtype'


c
c MAIN variables
c
        integer n, id_sele
        real*4  param(nc_max,nd_max), param0(nc_max,nd_max)

c
c RAN3
c 
        real*4 p, ran3


c
c Scratch variables
c
	integer ic
        real*4 xmin, xmax


        do ic=1,n 

          p=ran3(iseed0)

          xmin=apriori_info(id_sele,1)
          xmax=apriori_info(id_sele,2)

          param(ic,id_sele)=xmin+p*(xmax-xmin)

        enddo

        return
        end








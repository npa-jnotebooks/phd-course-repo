c
c ----------------------------------------------------------------------------
c
      subroutine write_last_synthetics
c
c ----------------------------------------------------------------------------
c
c


      include '../trans_dim.para'
      include '../modules/common.obs_syn'
      include '../modules/common.obs_syn2'
      include '../modules/common.recipe'
      include '../modules/common.param'




c Scratch variables

      integer id, ic, iadd
      real*4 mean1, std1, std, x, xmax, xmin
      character filename3*(namelen)
      character*6 id_lab
c
c
c

       write(*,*)
       write(*,*) '----------------------------------------------'
       write(*,*) ' WRITE SYNTHETICS from the LAST sampled model '
       write(*,*) '----------------------------------------------'
       write(*,*)


	write(*,*)
        write(*,*) ' WARNING:: Synthetics are created using white noise. '
        write(*,*) ' WARNING:: Standard deviation in file data.obs are   '
        write(*,*) ' WARNING:: scaled by:', std0
        write(*,*) 
c
c
c
c
c
c
         filename3='output/syn.dat'
         write(*,*) ' Writing file: ',filename3,' ...'
         open(iounit1,file=filename3,status='unknown')
         write(iounit1,'(a)') '# SYNTHETICS for DT'
	 write(iounit1,'(a)') '# n. of data '
         write(iounit1,'(i10)') ndata
         write(iounit1,'(a)') '#     LAT         LON         MOHO         ERR'
         iadd=0
         do id=1, ndata
            write(id_lab,'(i6)')id
            do ic=1,6
              if(id_lab(ic:ic).eq.' ')id_lab(ic:ic)='0'
            enddo
            mean1=syn_D0(id)
            std1=obs_D(id,4)
	    xmin=apriori_info(3,1)
	    xmax=apriori_info(3,2)

            if(added_noise)then
              std=std0*std1
              call sampleNormal(mean1,std,x)
              if(x.lt.xmin)then
                iadd=iadd+1
                x=xmin
              endif
              if(x.gt.xmax)then
                iadd=iadd+1
                x=xmax
              endif
              mean1=x
            endif

            write(iounit1,'(4f12.6)') obs_D(id,2), obs_D(id,1), mean1, std1

         enddo
         close(iounit1)

         if(iadd.gt.0)then
           write(*,*)
           write(*,'(a,i6)') ' --> DATA MODIFIED FOR BOUND COND VIOLATION: ', iadd
           write(*,*)
         endif





      return
      end


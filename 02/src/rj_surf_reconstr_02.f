c--------------------------------------------------------------------------------------------
c
c  RJ-SURF-RECONSTR::
c
c   02:: This algorithm performs:
c    --> standard Reversible-jump McMC
c
c--------------------------------------------------------------------------------------------
c
c  The Observed data:
c
c         obs_D(i,1)= X for the i-th point
c         obs_D(i,2)= Y coord for i-th point
c         obs_D(i,3)= MOHO DEPTH at the i-th point
c         obs_D(i,3)= error on MOHO DEPTH at the i-th point
c
c--------------------------------------------------------------------------------------------
c
c  The forward problem considers a Zero-order (i.e. constant value for the entire surface)
c  simple interpolation of the Voronoi ensamble over a 2D-space. The nearest Voronoi nucleus 
c  to a given point determines the Moho depth in that point, i.e.:
c
c    syn_D(i)=MOHO_near, 
c
c        where MOHO_near is the accelleration for the nearest nucleus to i-th point
c
c---------------------------------------------------------------------------------------------
c
c  The model: The physical parameterization is a Voronoi tasselization of the 2D space. Each
c  model is composed of a variable number N of Voronoi cells. Each cell is defined by its own
c  Voronoi nucleus, so, in practice, the model is represented by an N-ensemble of Voronoi
c  nuclei.
c
c     param -- N, x, y, acc  : 
c                           N- number of V nuclei
c                           x- coord,               |
c                           y- coord,               |-> param for one Voronoi nucleus
c                           MOHO- accelleration     | 
c
c---------------------------------------------------------------------------------------------
c
c
c   FLOW-CHART
c
c 
c   1. User initialization:
c   1.1 Read parameters for the random walk
c   1.2 Read parameters for the model space
c   1.3 Read observed data
c   1.4 Initialize integrals quantities

c   (start multi-chain inversion)
c    
c   1.5 Get initial model (model Zero)
c
c ------------------> this is the main Loop
c
c   2. Perturb the current model (or model Zero)
c
c   3. Compute Forward prediction
c
c   4. Compute Misfit between observed and predicted Accelleration
c
c   5. Metropolis's rule: substitute current model with candidate or keep it!
c
c   6. Write integrals (after BURN-IN phase)
c
c   7. Go back to point (2)
c
c ------------------> end of the main Loop
c
c   (end multi-chain inversion)
c
c   8. Finalize and exit
c
c

c----------------------------------------------------------------------------------------------



         program RJ_SURF_RECONSTR


         include 'trans_dim.para'

         include 'modules/common.obs_syn'
         include 'modules/common.param'
         include 'modules/common.randomtype'
         include 'modules/common.misfit'
         include 'modules/common.recipe'



c
c MAIN variables

	 integer n, n0
         real*4  param(nc_max,nd_max), param0(nc_max,nd_max)




c
c Scratch variables

         integer accepted, move_value
         integer ic, id, im, ichain
         real*4  lppd





c 
c   1. User initialization: 

c   1.1 Read parameters for the random walk

         write(*,*) ' ++RjSurfRec:: Read mcmc params ...'
         call read_mcmc_in

c   1.2 Read parameters for the model space

         write(*,*) ' ++RjSurfRec:: Read PARAMETER space info ...'
         call read_prior

c   1.3 Read observed data

         write(*,*) ' ++RjSurfRec:: Read OBSERVED data ...'
         call read_data

c   1.4 Initialize integrals quantities

         write(*,*) ' ++RjSurfRec:: initialize integrals ...'
         call integrals_bound



c
c MAIN LOOP 
c

         write(*,*) 
         write(*,*) ' ++RjSurfRec::' 
         write(*,*) ' ++RjSurfRec:: STARTING THE MAIN INVERSION LOOP'
         write(*,*) ' ++RjSurfRec:: '
         write(*,*)

c
c Start serial, multi-chain inversion
c 

         do ichain = 1, nchains


c   1.5 Get initial model (model Zero)

         write(*,*) ' ++RjSurfRec:: ramdomly pick the initial model ...'
         call get_initial_model(n0,param0)


         do im=1, ntot

           if(im.eq.1.or.mod(im,100).eq.0) write(*,'(2(a,i10),a,i5)')' ++RjSurfRec:: MODEL:',im,' out of ',ntot, ' chain:', ichain

           if(im.eq.1)then
c Model Zero becomes candidate model
             n=n0
             do ic=1,n
               do id=1,nd
                 param(ic,id)=param0(ic,id)
               enddo
             enddo
             move_value=1
           else

c
c   2. Perturb the current model (or model Zero)
c
             if(info) write(*,'(a)') ' ++RjSurfRec:: CANDIDATE SELECTION ...'
             call candidate_selection(n0,param0,n,param,move_value)

           endif
c
c   3. Compute Forward prediction
c
           if(info) write(*,'(a)') ' ++RjSurfRec:: FORWARD MODELLING ... '
           call forward(n,param)

c
c   4. Compute Misfit between observed and predicted time-series 
c
           if(info) write(*,'(a)') ' ++RjSurfRec:: MISFIT COMPUTATION ... '
           call misfit(lppd)

c
c   5. Metropolis's rule: substitute current model with candidate or keep it!
c
           if(info) write(*,'(a)') ' ++RjSurfRec:: Apply METROPOLIS rule ... '
           call metropolis(im,lppd,accepted)

           if(accepted.eq.0)then
c Candidate has been rejected -- current model becomes candidate model for integrals computation
             n=n0
             do ic=1,n
               do id=1,nd
                 param(ic,id)=param0(ic,id)
               enddo
             enddo
           endif
     
c
c   6. Write integrals (after BURN-IN phase) -- with chain thinning
c
           if(mod(im,nsample).eq.0)then

             write(99,202) 'MOD:',ichain,im,move_value, accepted,lppd,lppd0,n
             call write_accepted_model(n,param,ichain,im,move_value,accepted,lppd0)
           
 202       format(a,i6,i10,2i5,2f15.2,i6)

             if(im.ge.mod_burn_in)then

               call save_synthetics(accepted)
               call acceptance_statistics(move_value,accepted)
               call compute_integrals(n,param)

             endif

           endif

c Candidate model becomes current model

           n0=n
           do ic=1,n0
             do id=1,nd
               param0(ic,id)=param(ic,id)
             enddo
           enddo

c
c   7. Go back to point (2)
c
         enddo


c
c End serial, multi-chain inversion
c
         enddo




        
c        
c   8. Finalize and exit
c

        call write_mean_synthetics
        call write_last_synthetics
        call write_statistics
        call write_integrals


        stop
        end



        subroutine perturb_all_vnuc_properties(id_sele,n,param0,param)


        include '../trans_dim.para'


        include '../modules/common.obs_syn'
        include '../modules/common.param'
        include '../modules/common.recipe'
        include '../modules/common.randomtype'


c
c MAIN variables
c
        integer n, id_sele
        real*4  param(nc_max,nd_max), param0(nc_max,nd_max)


c
c Scratch variables
c
	integer ic
        real*4 x0, x_new, xmin, xmax, sc


        do ic=1,n 

          x0=param0(ic,id_sele)
          xmin=apriori_info(id_sele,1)
          xmax=apriori_info(id_sele,2)
          sc=sc_rmcmc(id_sele)

          call pick_cand_value_uniform(x0,xmin,xmax,sc,x_new)

          param(ic,id_sele)=x_new

        enddo

        return
        end








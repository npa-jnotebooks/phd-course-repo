

        subroutine perturb_vnuc_properties_from_priors(id_sele,ic_sele,param)


        include '../trans_dim.para'


        include '../modules/common.obs_syn'
        include '../modules/common.param'
        include '../modules/common.recipe'
        include '../modules/common.randomtype'


c
c MAIN variables
c
        integer id_sele,ic_sele
        real*4  param(nc_max,nd_max)


c
c Scratch variables
c
        real*4 xmin, xmax

c
c RAN3
c 
        real*4 p, ran3

        p=ran3(iseed0)

        xmin=apriori_info(id_sele,1)
        xmax=apriori_info(id_sele,2)

        param(ic_sele,id_sele)=xmin+p*(xmax-xmin)

        return
        end








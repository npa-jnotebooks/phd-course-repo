c
c
c       subroutine CANDIDATE_SELECTION
c
c---------------------------------------------------------------------------------
c
c INPUT:
c         n0  ------ Number of Voronoi nuclei current model
c         param0 -- parameter values for each Voronoi nucleus in curent model
c
c OUTPUT:
c         n  ------ Number of Voronoi nuclei in candidate model
c         param -- parameter values for each Voronoi nucleus in candidate model
c         move_value -- selected MOVE in the recipe
c
c---------------------------------------------------------------------------------
c
c 03: RECIPE includes five moves, each diffrent probability:
c
c
c (1) Perturb hyper-parameter for data uncertainties (move_value=1)
c (2) Perturb X of one vnuc (move_value=2)
c (3) Perturb Y of one vnuc (move_value=3)
c (4) Perturb MOHO, different recipes available (move_value=4)
c (5) ADD a new Voronoi cell (move_value=5)
c (6) REMOVE an existent Voronoi cell (move_value=6)
c
c
c---------------------------------------------------------------------------------
c
c
        subroutine candidate_selection(n0,param0,hparam0,n,param,hparam,move_value)

        include '../trans_dim.para'


        include '../modules/common.obs_syn'
        include '../modules/common.param'
        include '../modules/common.recipe'
        include '../modules/common.randomtype'


c
c MAIN variables
c
        integer n, n0, move_value
	real*4  hparam(max_n_hparam), hparam0(max_n_hparam)
        real*4  param(nc_max,nd_max), param0(nc_max,nd_max)


c
c Scratch variables
c 
        integer imove,  ic, id, ic_sele, n_cand
        real*4 min_prob, max_prob


c
c RAN3 variables
c
        real*4 p, ran3




c
c 1. CANDIDATE MODEL IS EQUAL TO CURRENT (and after, it is perturbed)
c 
        n=n0
        do ic=1,n
          do id=1,nd
            param(ic,id)=param0(ic,id)
          enddo
        enddo
	hparam(1)=hparam0(1)

        if(info)then

          write(*,*) 
          write(*,*)
          write(*,'(a)') ' ++RjSurfRec:: CANDIDATE SELECTION -- '
          write(*,'(a)') ' ++RjSurfRec:: CURRENT MODEL: '
          do ic=1,n
            write(*,'(a,i4,3f16.4)') ' ++RjSurfRec:: NUCLEUS: ',ic,(param(ic,id), id=1,nd)
          enddo
	  write(*,'(a)') ' ++RjSurfRec:: H-PARAM::', hparam(1)
          write(*,*)

        endif




c
c 2. SELECT A MOVE
c
        p=ran3(iseed0)

        min_prob=0.0
        max_prob=0.0
        move_value=-1

        do imove=1, nmoves
          min_prob=max_prob
          max_prob=min_prob+move_prob(imove)
          if(min_prob.lt.p.and.p.le.max_prob.and.move_value.lt.0)then
            move_value=imove
          endif
        enddo

	if(move_value.eq.-1)move_value=nmoves

        if(info) write(*,*) ' ++ RjSurfRec:: CAND-SELE -- Selected move:', move_value




c
c
c 3. PERTURB CANDIDATE
c
c
c
c MOVE_VALUE=1 -- hyper-parameter perturb

        if(move_value.eq.1)then

          call perturb_hparam(hparam0,hparam)

c
c
c MOVE_VALUE=2 -- x perturb

        else if(move_value.eq.2)then

          call select_vnuc(n,ic_sele)
          call perturb_vnuc_pos(1,ic_sele,n,param0,param)
c
c
c MOVE_VALUE=3 -- y perturb

        else if(move_value.eq.3)then

          call select_vnuc(n,ic_sele)
          call perturb_vnuc_pos(2,ic_sele,n,param0,param)
c
c
c MOVE_VALUE=4 -- MOHO perturb

        else if(move_value.eq.4)then

          if(perturb_one_vnuc)then

	    call select_vnuc(n,ic_sele)
            call perturb_vnuc_properties(3,ic_sele,param0,param)

          else if(perturb_one_vnuc_from_prior)then

            call select_vnuc(n,ic_sele)
            call perturb_vnuc_properties_from_priors(3,ic_sele,param0,param)

          else if(perturb_all_vnuc)then

            call perturb_all_vnuc_properties(3,n,param0,param)

          else if(perturb_all_vnuc_from_prior)then

	    call perturb_all_vnuc_properties_from_priors(3,n,param0,param)

	  endif

c
c
c MOVE_VALUE=5/6 -- Birth/Death of a Voronoi nucleus

        else if(move_value.eq.5)then

          call pick_cand_n_value_uniform(n0,min_c,max_c,n_cand)
          n=n_cand
         
          if(n.lt.n0)then

            move_value=4
          
            if(info) write(*,'(2(a,i4))') ' ++RjSurfRec:: DEATH MOVE:', n0, ' --> ', n

            call select_vnuc(n0,ic_sele)
            call death_vnuc(ic_sele,param0,n,param)

          endif

          if(n.gt.n0)then

             move_value=5

             if(info) write(*,'(2(a,i4))') ' ++RjSurfRec:: BIRTH: MOVE:', n0, ' --> ', n

             call birth_vnuc(n0,param0,n,param)

          endif

          if(n.eq.n0)then
            if(info) write(*,'(a,i4)') ' ++RjSurfRec:: MODEL IS NOT PURTURBED -- move_value=',move_value
          endif







        endif





        if(info)then

          write(*,*)
          write(*,'(a)') ' ++RjSurfRec:: CANDIDATE MODEL: '
          do ic=1,n
            write(*,'(a,i4,3f16.4)') ' ++RjSurfRec:: NUC: ',ic,(param(ic,id), id=1,nd)
          enddo
          write(*,'(a)') ' ++RjSurfRec:: H-PARAM::', hparam(1)
          write(*,*)


        endif






        return
        end

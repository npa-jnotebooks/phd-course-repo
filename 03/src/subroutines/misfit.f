c
c
c	subroutine MISFIT
c
c---------------------------------------------------------------------------
c
c INPUT:
c        hparam  --  scaling factor for the STD
c
c OUTPUT:
c         lh_norm -- logaritmic value of the normalizing factor in the PPD
c         lppd -- logarithmic value of the PPD (i.e Chi-squared value)
c
c---------------------------------------------------------------------------
c
c
        subroutine misfit(hparam,lh_norm,lppd)

        

        include '../trans_dim.para'


        include '../modules/common.obs_syn'
        include '../modules/common.param'
        include '../modules/common.recipe'


c
c MAIN variables

	real*4  hparam(max_n_hparam)
        real*4  lppd, lh_norm



c
c Scratch variables

        integer id
        real*4  diff, std

       
c
c COMPUTE FIT AS A CHI-SQUARE VALUE. COVARIANCE ERROR MATRIX IS CONSIDERED DIAGONAL
c


        diff=0.e0

        do id=1,ndata

c Misfit over ACCELLERATION

	    std=(10**hparam(1))*obs_D(id,4)
            diff = diff + ((obs_D(id,3)-syn_D(id))/std)**2
	    if(info) write(*,'(a,5f12.4)') '++Misfit:: ', obs_D(id,3), syn_D(id), obs_D(id,4), diff

        enddo


	lh_norm=1.0*ndata*hparam(1)*2.302585
        lppd=diff

       

        return
        end

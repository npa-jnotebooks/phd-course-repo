

        subroutine perturb_hparam(hparam0,hparam)


        include '../trans_dim.para'


        include '../modules/common.obs_syn'
        include '../modules/common.param'
        include '../modules/common.recipe'
        include '../modules/common.randomtype'


c
c MAIN variables
c
        real*4  hparam(max_n_hparam), hparam0(max_n_hparam)


c
c Scratch variables
c
        real*4 x0, x_new, xmin, xmax, sc


        x0=hparam0(1)
        xmin=apriori_info_hparam(1,1)
        xmax=apriori_info_hparam(1,2)
        sc=sc_rmcmc_hparam(1)

        call pick_cand_value_uniform(x0,xmin,xmax,sc,x_new)

        hparam(1)=x_new



        return
        end








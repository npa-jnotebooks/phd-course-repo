

        subroutine read_data


c Read data file: input/OBS/data.obs and store value in COMMON BLOCK

        include '../trans_dim.para'


        include '../modules/common.obs_syn'
        include '../modules/common.param'

c
c Scratch Variables 

        integer  id, id0, ndata0
	real*4 lat, lon, moho, err


        open(12,file='input/OBS/data.obs',status='old')

        read(12,*)
        read(12,*)
c Read NUMBER OF DATA
        read(12,*) ndata0

        read(12,*) 
	id=0
        do id0=1,ndata0
          read(12,*) lat,lon,moho,err
          if(err.lt.min_std)err=min_std
	  id=id+1
	  obs_D(id,1)=lon
          obs_D(id,2)=lat
          obs_D(id,3)=moho
	  obs_D(id,4)=err
        enddo

	ndata=id

c
c -----------------------------------------------------------------------
c WRITE OUT OBSERVED DATA for REFERENCE
c
        open(13,file='output/obs.data',status='unknown')

        do id=1,ndata
          write(13,101) 'OBS DATA:', id, obs_D(id,1),obs_D(id,2),obs_D(id,3),obs_D(id,4)
        enddo

	close(13)

 101    format(a,i6,4f16.5)
c
c -----------------------------------------------------------------------
c


        close(12)


        return
        end



c
c ----------------------------------------------------------------------------
c
      subroutine write_mean_synthetics
c
c ----------------------------------------------------------------------------
c
c  Input/output: (none) -- all quantities are read from common blocks
c
c ----------------------------------------------------------------------------
c


      include '../trans_dim.para'
      include '../modules/common.obs_syn'
      include '../modules/common.obs_syn2'
      include '../modules/common.recipe'


c Scratch variables
      integer id
      integer tot
      real*4 mean1, std1, x
      character filename3*(namelen)
c
c
c

       write(*,*)
       write(*,*) '++++++++++++++++++++++++++++++++++++++++++++++'
       write(*,*) '   WRITE MEAN SYNTHETICS for ACC '
       write(*,*) '++++++++++++++++++++++++++++++++++++++++++++++'
       write(*,*)
c
c
         tot=int((ntot-mod_burn_in)/nsample)*nchains
         write(*,*) ' Total collected models [(NTOT-BURN_IN)/NSAMPLE] :', tot
c
c
c
c
         filename3='output/mean_syn.dat'
         write(*,*) ' Writing file: ',filename3,' ...'
         open(iounit1,file=filename3,status='unknown')
         write(iounit1,'(a)') '# MEAN SYNTHETICS for DT and THETA '
         do id=1, ndata
            mean1=mean_syn_D(id)/tot
            x=(mean_syn_D2(id)/tot)-mean1**2
            if(x.lt.1e-8)x=1.e-8
            std1=sqrt(x)
            write(iounit1,'(i5,4f12.3,i8)')id, obs_D(id,1), obs_D(id,2), mean1, std1, n_samples(id)
         enddo
         close(iounit1)





      return
      end

c

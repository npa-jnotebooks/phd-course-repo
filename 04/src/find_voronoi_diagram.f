




	PROGRAM FIND_VORONOI_DIAGRAM

	implicit none

	integer nc, nx, ny
	real*4 xmin, xmax, ymin, ymax
	real*4 deltax, deltay
	real*4 model(1000,3)
	real*4 x, y, acc, min_dist, dist

	integer ic, id, ix, iy


	open(10,file='value.tmp',status='old')
	read(10,*) xmin, xmax, ymin, ymax
	read(10,*) nc, nx, ny
	close(10)

	open(10,file='model.tmp',status='old')
	do ic=1,nc
	  read(10,*) (model(ic,id),id=1,3)
	enddo
	close(10)


	deltax=(xmax-xmin)/(1.0*nx-1.0)
        deltay=(ymax-ymin)/(1.0*ny-1.0)

	open(10,file='voronoi.xyz.tmp',status='unknown')

	do ix=1,nx
	
	  x=xmin+(1.0*ix-1.0)*deltax
	
	do iy=1,ny

	  y=ymin+(1.0*iy-1.0)*deltay

	  acc=-1.0
	  min_dist=10000.
	  do ic=1,nc

	    dist=sqrt( (x-model(ic,1))**2 + (y-model(ic,2))**2 )
	    if(dist.lt.min_dist)then
              acc=model(ic,3)
	      min_dist=dist
            endif
          enddo

          write(10,'(3f16.6)') x, y, acc

       enddo

       enddo





	stop
	end


        subroutine birth_vnuc(n0,param0,n,param)


        include '../trans_dim.para'

        include '../modules/common.obs_syn'
        include '../modules/common.param'
        include '../modules/common.recipe'
        include '../modules/common.randomtype'


c
c MAIN variables
c
        integer n0, n
        real*4  param(nc_max,nd_max), param0(nc_max,nd_max)


c
c Scratch variables
c
        integer it, ic, ip
        real*4 dist, x_new(2)
        logical too_close

c
c RAN3 variables
c
        real*4 p, ran3



c A new complexity is created

c Select a new position

        do it=1,1000

	  do ip=1,2
            p=ran3(iseed0)
            x_new(ip)=apriori_info(ip,1)+p*(apriori_info(ip,2)-apriori_info(ip,1))
          enddo

          too_close=.false.
          do ic=1,n0
            dist=sqrt( (param0(ic,1)-x_new(1))**2 + (param0(ic,2)-x_new(2))**2 )
            if(dist.lt.min_dist_nuc) too_close=.true.
          enddo

          if(.not.too_close)then
            param(n,1)=x_new(1)
	    param(n,2)=x_new(2)
            goto 202
          endif

        enddo

 202    continue

c Select new values for DT from PRIOR

 
        p=ran3(iseed0)
        param(n,3)= apriori_info(3,1)+p*(apriori_info(3,2)-apriori_info(3,1))




        return
        end








      subroutine integrals_bound
c
c integrals_bound_1d -- computes min/max values for integrals 
c
      
      include '../trans_dim.para'
      include '../modules/common.param'
      include '../modules/common.integral'
      include '../modules/common.recipe'





c
c Scratch variables
      integer ix, iy





c
c MOHO and Nuclei density Properties
c


      do ix=1, nstep_map
	do iy=1, nstep_map

          bound_grid(ix,iy,1,1)= apriori_info(3,1)
          bound_grid(ix,iy,1,2)= apriori_info(3,2)

	enddo
      enddo




      return
      end


c----------------------------------------------------------------------------------------
c
c
c
c

c
      subroutine compute_integrals(n,param)
c
c compute_integrals -- computes integrals 
c

      
      include '../trans_dim.para'
      include '../modules/common.param'
      include '../modules/common.recipe'
      include '../modules/common.obs_syn'
      include '../modules/common.integral'



      integer n
      real*4  param(nc_max,nd_max)
c
c
c
c Scratch variables
      integer ic, ix, iy, idiv, ic_nearest
      real*4  min_p, max_p, delta_y, y, delta_x, x
      real*4  dist, dist_x, dist_y, acc, dist_min



         
c
c MOHO and DENSITY
c

      delta_x=1.0*((x_max-x_min)/(nstep_map-1))
      delta_y=1.0*((y_max-y_min)/(nstep_map-1))

      do ix=1, nstep_map

	x=x_min+(ix-1)*delta_x

      do iy=1, nstep_map

        y=y_min+(iy-1)*delta_y

c Nearest complexity

           ic_nearest=-1
           dist_min=10e20

           do ic=1,n

             dist=sqrt((param(ic,1)-x)**2+(param(ic,2)-y)**2)

             if(dist.lt.dist_min)then
               dist_min=dist
               ic_nearest=ic
             endif

           enddo

c Local MOHO

           acc=param(ic_nearest,3)
           min_p=bound_grid(ix,iy,1,1)
           max_p=bound_grid(ix,iy,1,2)

           idiv=int(ndiv*(acc-min_p)/(max_p-min_p))+1

           if(idiv.le.0)idiv=1
           if(idiv.gt.ndiv)idiv=ndiv

           int_grid(ix,iy,1,idiv)=int_grid(ix,iy,1,idiv)+1

c Local Density

           ic_nearest=0
         
           do ic=1,n

             dist_x=abs(param(ic,1)-x)
	     dist_y=abs(param(ic,2)-y)

             if(dist_x.lt.(1.0*delta_x).and.dist_y.lt.(1.0*delta_y))then         
               ic_nearest=ic_nearest+1
             endif

           enddo

	   int_grid_density(ix,iy)=int_grid_density(ix,iy)+ic_nearest          

      enddo

      enddo


      return
      end


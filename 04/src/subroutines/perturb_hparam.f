

        subroutine perturb_hparam(hparam0,hparam)


        include '../trans_dim.para'


        include '../modules/common.obs_syn'
        include '../modules/common.param'
        include '../modules/common.recipe'
        include '../modules/common.randomtype'


c
c MAIN variables
c
        real*4  hparam(max_n_hparam), hparam0(max_n_hparam)


c
c Scratch variables
c
	integer ip, ip_sele
        real*4 x0, x_new, xmin, xmax, sc
	real*4 min_prob, max_prob, delta_prob

c
c RAN3 variables
c
        real*4 p, ran3




c Select HPARAM

        p=ran3(iseed0)
        min_prob=0.0
        max_prob=0.0
        ip_sele=-1
        delta_prob=1.0/(1.0*nhparam)

        do ip=1, nhparam
          min_prob=max_prob
          max_prob=min_prob+delta_prob
          if(min_prob.lt.p.and.p.le.max_prob.and.ip_sele.lt.0)then
            ip_sele=ip
            if(info) write(*,*) ' ++ RjSurfRec:: CAND-SELE -- SELECTED HPARAM:', ip_sele
          endif
        enddo

        if(ip_sele.lt.0) ip_sele=nhparam



        x0=hparam0(ip_sele)
        xmin=apriori_info_hparam(ip_sele,1)
        xmax=apriori_info_hparam(ip_sele,2)
        sc=sc_rmcmc_hparam(ip_sele)

        call pick_cand_value_uniform(x0,xmin,xmax,sc,x_new)

        hparam(ip_sele)=x_new



        return
        end








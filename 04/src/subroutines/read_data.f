

        subroutine read_data


c Read data file: input/OBS/data.obs and store value in COMMON BLOCK

        include '../trans_dim.para'


        include '../modules/common.obs_syn'
        include '../modules/common.param'

c
c Scratch Variables 

        integer  id, id0, ndata0, ic, is
	real*4 lat, lon, moho, err
	character*22 filename1
	character*19 filename2
	character*3 id_char


	do is=1,nhparam

          write(id_char,'(i3)') is
          do ic=1,3
            if(id_char(ic:ic).eq.' ') id_char(ic:ic)='0'
          enddo


	  filename1='input/OBS/data.obs.'//id_char
          open(12,file=filename1,status='old')

          read(12,*)
          read(12,*)
c Read NUMBER OF DATA
          read(12,*) ndata0

          read(12,*) 
	  id=0
          do id0=1,ndata0
	    id=id+1
	    read(12,*) lat,lon,moho,err
	    if(err.lt.min_std)err=min_std
	    obs_D(is,id,1)=lon
            obs_D(is,id,2)=lat
            obs_D(is,id,3)=moho
	    obs_D(is,id,4)=err
          enddo


	  ndata(is)=id

c
c -----------------------------------------------------------------------
c WRITE OUT OBSERVED DATA for REFERENCE
c
	  filename2='output/obs.data.'//id_char
          open(13,file=filename2,status='unknown')

          do id=1,ndata(is)
            write(13,101) 'OBS DATA:', is, id, obs_D(is,id,1),obs_D(is,id,2),obs_D(is,id,3),obs_D(is,id,4)
          enddo

	  close(13)

 101      format(a,2i6,4f16.5)
c
c -----------------------------------------------------------------------
c


          close(12)

	enddo


        return
        end



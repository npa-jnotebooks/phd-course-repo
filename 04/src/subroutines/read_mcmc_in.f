

        subroutine read_mcmc_in


c Read parameter file: rmcmc.in and store value in COMMON BLOCK

        include '../trans_dim.para'

        include '../modules/common.recipe'
        include '../modules/common.randomtype'

c
c Scratch Variables

        integer yesno, im





        open(12,file='mcmc.in',status='old')

        read(12,*)
c Read NUMBER OF INDEPENDENT CHAINS
        read(12,*)nchains
c Read NUMBER OF MODELS IN THE McMC walk
        read(12,*)ntot
c Read NUMBER OF MODELS IN THE BURN-IN phase
        read(12,*)mod_burn_in
c Read THINNING CHAIN parameter (i.e. only 1/nsample models are saved to inegrals)
        read(12,*)nsample
c Read if POSTERIOR or PRIOR is sampled
        posterior=.false.
        read(12,*)yesno
        if(yesno.eq.0)posterior=.true.
c Read RANDOMSEED
        read(12,*) iseed0
c Read LEVEL of OUTPUT (1- all; 0- nothing)
        info=.false.
        read(12,*)yesno
        if(yesno.eq.1)info=.true.

        read(12,*)
c Read MOVE PROB for the two moves in the McMC walk
	read(12,*)nmoves	
	do im=1,nmoves
          read(12,*)move_prob(im)
	enddo

        perturb_one_vnuc=.false.
        perturb_one_vnuc_from_prior=.false.
        perturb_all_vnuc=.false.
        perturb_all_vnuc_from_prior=.false.

        read(12,*)
c Read RECIPE for PROPERTIES PERTUBATION (0-weakest ; ...; 3- Strongest)
        read(12,*)yesno
        if(yesno.eq.0)perturb_one_vnuc=.true.
        if(yesno.eq.1)perturb_one_vnuc_from_prior=.true.
        if(yesno.eq.2)perturb_all_vnuc=.true.
        if(yesno.eq.3)perturb_all_vnuc_from_prior=.true.


        read(12,*)
c Read PARAMETER for INTEGRATION
        read(12,*)nstep
        read(12,*)nstep_map
        ndiv=nstep

        close(12)


c
c OPEN log file
c
        open(99,file='output/chain.log',status='unknown') 
        write(99,'(a)') 'MOD:     CH      MOD   MOVE  ACCEPT   LH_NORM       LH_NORM0           LPPD          LPPD0     N       PI(1)    PI(2)'


c
c OPEN PPD model file
c
        open(98,file='output/samples.dat',status='unknown')

 


        return
        end





        subroutine read_prior


c Read parameter file: input/MDL/prior.info and store value in COMMON BLOCK

        include '../trans_dim.para'

        include '../modules/common.param'
        include '../modules/common.recipe'
        include '../modules/common.obs_syn'


        integer yesno, ip
c


        open(12,file='input/MDL/prior.info',status='old')

        read(12,*)
        read(12,*)
        read(12,*)
        read(12,*)

c Read Number of complexities    
        read(12,*) min_c, max_c
c Read MIN-DIST between complexities    (WARN:: this must be in coordinate unit, it could be km or degree)
        read(12,*)min_dist_nuc
c Read the NUMBER OF DIMENSIONS of each complexity
        read(12,*)nd

        read(12,*)

c Read MIN/MAX/SCALE for X dimension 
        read(12,*)apriori_info(1,1),apriori_info(1,2),sc_rmcmc(1)
	x_min=apriori_info(1,1)
	x_max=apriori_info(1,2)

c Read MIN/MAX/SCALE for Y dimension 
        read(12,*)apriori_info(2,1),apriori_info(2,2),sc_rmcmc(2)
        y_min=apriori_info(2,1)
        y_max=apriori_info(2,2)

c Read MIN/MAX/SCALE for ACC  dimension       
        read(12,*)apriori_info(3,1),apriori_info(3,2),sc_rmcmc(3)

	read(12,*)
        read(12,*)
        read(12,*) nhparam
c Read MIN/MAX/SCALE for hyper-parameter
	do ip=1,nhparam
          read(12,*)apriori_info_hparam(ip,1),apriori_info_hparam(ip,2),sc_rmcmc_hparam(ip)
	enddo

c Read MIN standard deviation for a data-point

        read(12,*)
        read(12,*)
	read(12,*) min_std

c Read if noise has to be added to synthetics

        added_noise=.false.
        read(12,*)
        read(12,*)
        read(12,*)yesno, std0
        if(yesno.eq.1)added_noise=.true.

        close(12)







        return
        end



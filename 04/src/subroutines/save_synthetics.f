

      subroutine save_synthetics(iacc)

c
c ------------------------------------------------------------------------------------
c This subroutine saves the values of the synthetic Depth and their square
c to compute mean posterior values.
c ------------------------------------------------------------------------------------
c
c   Input:
c         iacc      -- ACCEPTED flag
c
c   Output:
c        (none)     -- SYN phases are stored in a common block
c
c ------------------------------------------------------------------------------------
c


      include '../trans_dim.para'
      include '../modules/common.param'
      include '../modules/common.obs_syn'
      include '../modules/common.obs_syn2'

c Main variables
      integer iacc

c Scratch variables
        integer id, is


	do is=1,nhparam

        if(iacc.ne.0)then

           do id = 1, ndata(is)
              syn_D0(is,id)=syn_D(is,id)
           enddo

        endif

        do id = 1, ndata(is)
              mean_syn_D(is,id)=mean_syn_D(is,id)+syn_D0(is,id)
              mean_syn_D2(is,id)=mean_syn_D2(is,id)+syn_D0(is,id)**2
        enddo

	enddo

      return
      end
c
c
c

c
c ----------------------------------------------------------------------------


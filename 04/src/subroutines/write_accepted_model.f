c
c ----------------------------------------------------------------------------
c
      subroutine write_accepted_model(n,param,hparam,ichain,imod,move_value,accepted,lppd0)
c
c write_statistics -- write MOVE statistics to file
c
c
c

      include '../trans_dim.para'
      include '../modules/common.rmcmc_stat'
      include '../modules/common.recipe'
      include '../modules/common.param'

      integer n, ichain, imod, move_value, accepted
      integer ic, id

      real*4  lppd0, param(nc_max,nd_max)
      real*4  hparam(max_n_hparam)


      write(98,'(A)')' MODfromPPD:: +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
      write(98,'(A,i6,i10,2i8)') ' MODfromPPD:: STAT: ',ichain, imod, move_value, accepted
      write(98,'(A,i6,i10,4f16.4)') ' MODfromPPD:: STAT: ',ichain, imod, lppd0
      write(98,'(A,i6,i10,3i12)') ' MODfromPPD:: STAT: ',ichain, imod, n
      do ic=1,n
        write(98,'(A,i6,i10,i8,20f16.4)') ' MODfromPPD:: PARA: ',ichain, imod, ic, (param(ic,id),id=1,nd)
      enddo
      write(98,'(A)')' MODfromPPD:: +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'

      return
      end
c
c


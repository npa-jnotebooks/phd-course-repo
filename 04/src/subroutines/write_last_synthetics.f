c
c ----------------------------------------------------------------------------
c
      subroutine write_last_synthetics
c
c ----------------------------------------------------------------------------
c
c


      include '../trans_dim.para'
      include '../modules/common.obs_syn'
      include '../modules/common.obs_syn2'
      include '../modules/common.recipe'
      include '../modules/common.param'




c Scratch variables

      integer id, iadd, is, ic
      real*4 mean1, std1, std, x, xmax, xmin
      character filename3*(namelen)
      character*6 id_lab
      character*3 id_char
c
c
c

       write(*,*)
       write(*,*) '----------------------------------------------'
       write(*,*) ' WRITE SYNTHETICS from the LAST sampled model '
       write(*,*) '----------------------------------------------'
       write(*,*)


	write(*,*)
        write(*,*) ' WARNING:: Synthetics are created using white noise. '
        write(*,*) ' WARNING:: Standard deviation in file data.obs are   '
        write(*,*) ' WARNING:: scaled by:', std0
        write(*,*) 
c
c
c
c
c
c

	 do is=1,nhparam

         write(id_char,'(i3)') is
         do ic=1,3
           if(id_char(ic:ic).eq.' ') id_char(ic:ic)='0'
         enddo

         filename3='output/syn.'//id_char//'.dat'
         write(*,*) ' Writing file: ',filename3,' ...'
         open(iounit1,file=filename3,status='unknown')
         write(iounit1,'(a)') '# SYNTHETICS for MOHO'
         write(iounit1,'(a)') '# n. of data '
         write(iounit1,'(i10)') ndata(is)
         write(iounit1,'(a)') '#     LAT         LON         MOHO         ERR'
         iadd=0
         do id=1, ndata(is)
            write(id_lab,'(i6)')id
            do ic=1,6
              if(id_lab(ic:ic).eq.' ')id_lab(ic:ic)='0'
            enddo
            mean1=syn_D0(is,id)
            std1=obs_D(is,id,4)
            xmin=apriori_info(3,1)
            xmax=apriori_info(3,2)

            if(added_noise)then
              std=std0*std1
              call sampleNormal(mean1,std,x)
              if(x.lt.xmin)then
                iadd=iadd+1
                x=xmin
              endif
              if(x.gt.xmax)then
                iadd=iadd+1
                x=xmax
              endif
              mean1=x
            endif

            write(iounit1,'(4f12.6)') obs_D(is,id,2), obs_D(is,id,1), mean1, std1

         enddo
         close(iounit1)

         if(iadd.gt.0)then
           write(*,*)
           write(*,'(a,i6)') ' --> DATA MODIFIED FOR BOUND COND VIOLATION: ', iadd
           write(*,*)
         endif

	 enddo







      return
      end


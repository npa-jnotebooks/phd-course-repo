c
c ----------------------------------------------------------------------------
c
      subroutine write_mean_synthetics
c
c ----------------------------------------------------------------------------
c
c  Input/output: (none) -- all quantities are read from common blocks
c
c ----------------------------------------------------------------------------
c


      include '../trans_dim.para'
      include '../modules/common.obs_syn'
      include '../modules/common.obs_syn2'
      include '../modules/common.recipe'
      include '../modules/common.param'


c Scratch variables
      integer id,  ic, is
      integer tot
      real*4 mean1, std1, x
      character filename3*(namelen)
      character*3 id_char
c
c
c

       write(*,*)
       write(*,*) '++++++++++++++++++++++++++++++++++++++++++++++'
       write(*,*) '   WRITE MEAN SYNTHETICS for ACC '
       write(*,*) '++++++++++++++++++++++++++++++++++++++++++++++'
       write(*,*)
c
c
         tot=int((ntot-mod_burn_in)/nsample)*nchains
         write(*,*) ' Total collected models [(NTOT-BURN_IN)/NSAMPLE] :', tot
c
c
c
c
         do is=1,nhparam

         write(id_char,'(i3)') is
         do ic=1,3
           if(id_char(ic:ic).eq.' ') id_char(ic:ic)='0'
         enddo

         filename3='output/mean_syn.'//id_char//'.dat'
         write(*,*) ' Writing file: ',filename3,' ...'
         open(iounit1,file=filename3,status='unknown')
         write(iounit1,'(a)') '# MEAN SYNTHETICS for ACC '
         do id=1, ndata(is)
            mean1=mean_syn_D(is,id)/tot
            x=(mean_syn_D2(is,id)/tot)-mean1**2
            if(x.lt.1e-8)x=1.e-8
            std1=sqrt(x)
            write(iounit1,'(i5,4f12.3,i8)')id, obs_D(is,id,1), obs_D(is,id,2), mean1, std1, n_samples(is,id)
         enddo
         close(iounit1)

	 enddo





      return
      end

c

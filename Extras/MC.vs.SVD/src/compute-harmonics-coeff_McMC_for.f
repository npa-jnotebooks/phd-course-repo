c
c
c
       PROGRAM comp_harmo
c
c COMPUTE_HARMO_COEFF: this code computes angular harmonic coefficents for a R- and T- RF data-set. Here, I followed a
c Monte Carlo approach and developed a Markov chain (via Metropolis' rule) to sample the parameter space defined
c by the angular coefficent. A L2-misfit is used to define the fit between a "model" (i.e a 5-value vector: A, B, C, D and E)
c and the observed data. The Mean and standard deviation fo the posterior PDF are used as final value of the 5 parameters and
c their associated errors.
c
c
c
       implicit none
      
       include 'common.randomtype'

       integer i, lw, n_bin, lw_dir, lw_name
       integer icomp, isamp, ibin, yesno
       integer n_samp, n_header_lines, n_bin_r, n_bin_t
       real*4 phi, shift
       character*128 filename
       character*128 filename2
       character*128 binname
       character*128 bindir
       character*1 comp, k_c
       character*3 phi_c, pol_c, comp2

       integer max_mod, maxharmo
       parameter (max_mod=1000000)
       parameter (maxharmo=2)
       parameter (n_header_lines=1)

       real*4 min_para(2*maxharmo+1), max_para(2*maxharmo+1), delta_para
       logical gaussian(2*maxharmo+1)

       real*4 para(2*maxharmo+1), para0(2*maxharmo+1), lppd, lppd0

       integer max_bin, maxsamples
       parameter (max_bin=50)
       parameter (maxsamples=1500)
       real*4 bin(max_bin,2,maxsamples,2)
       real*4 std_bin(max_bin,2,maxsamples,2)
       real*4 harm(0:2,2,maxsamples)
       real*4 std_harm(0:2,2,maxsamples)
       real*4 baz(max_bin), dist(max_bin)

       real*4 deg2rad
       parameter (deg2rad=0.01745329)

       integer ntot, mod_burn_in
       logical posterior

       real*4 obs(2*max_bin)
       real*4 syn(2*max_bin)
       real*4 std_obs(2*max_bin)
       real*4 H(2*max_bin,2*maxharmo+1)

       real*4 sample(2*maxharmo+1,max_mod)
       real*4 C(2*maxharmo+1,maxsamples,2)
       real*4 std_C(2*maxharmo+1,maxsamples,2)



       real*4 mean, std

       integer ik, jk, jbin, irow, ib, n_iter, it, ip, imod, iacc
       real*4  p, ran3, alpha
       real*4  x0, x_new, xmin, xmax, sc, xmean, xstd

       integer lofw

c
c FILE FOR LOG
c
        open(99, file='output/McMC/log.out',status='unknown')




c
c READ INPUT FILES
c

        open(12,file='mcmc.in',status='old')
        read(12,*)
        read(12,*)ntot
        read(12,*)mod_burn_in
        posterior=.false.
        read(12,*)yesno
        if(yesno.eq.0)posterior=.true.
        read(12,*)delta_para
        read(12,*) iseed0
        close(12)



c
c READ OBSERVED DATA AND ERRORS
c
        open(13,file='output/McMC/harm.coeff.geom-coverage.dat',status='unknown')
        write(13,*)'# GEOMETRICAL COVERAGE (BAZ and DIST)'

        do icomp=1,2
          
          if(icomp.eq.1)then
             open(10,file='input/OBS_DATA/list.bin.R.harmo.coeff',status='old')
          endif
          if(icomp.eq.2)then
             open(10,file='input/OBS_DATA/list.bin.T.harmo.coeff',status='old')
          endif
                                                                      
          do ibin = 1, max_bin

           read(10,*,END=101) binname, baz(ibin), dist(ibin)
           write(*,*)' BIN NAME: ', binname, ' from baz/dist: ', baz(ibin),'/', dist(ibin)
           if(icomp.eq.1)write(13,'(2f15.5)') baz(ibin), dist(ibin)
           lw_name=lofw(binname)
c  Read and Load comp of the bin (and its STD) in bin matrix
           filename= 'input/OBS_DATA/'//binname(1:lw_name)
           filename2='input/OBS_DATA/SIGMA_smo_'//binname(1:lw_name)
           write(*,*) 'Open: ', filename
           open(11,file=filename, status='old')
           open(12,file=filename2,status='old')
           do i=1,n_header_lines
                read(11,*)
                read(12,*)
           enddo
           write(*,*) ' reading file ...'
           do isamp= 1, maxsamples
                read(11,*, END=201)bin(ibin,icomp,isamp,1), bin(ibin,icomp,isamp,2)
                read(12,*, END=201)bin(ibin,icomp,isamp,1), std_bin(ibin,icomp,isamp,2)
           enddo
 201       n_samp=isamp-1
           close(11)

          enddo
          
 101      n_bin=ibin-1
          if(icomp.eq.1)n_bin_r=ibin-1
          if(icomp.eq.2)n_bin_t=ibin-1
          close(10)
        enddo
          if(n_bin_t.ne.n_bin_r)
     &       STOP ' - different number of bins in the two list...'

        

        close(13)


c
c DEFINE PRIOR INFORMATION
c
        write(*,'(a)')
        write(*,'(a)') '# Define PRIOR INFO:'
        write(*,'(a)') '# MIN/MEAN    MAX/STD  GAUSS/UNIF'
        open(12,file='input/MDL/prior.info',status='old')
        read(12,*)
        read(12,*)
        do ip=1,2*maxharmo+1
          gaussian(ip)=.false.
          read(12,*) min_para(ip), max_para(ip), yesno
          if(yesno.eq.1)gaussian(ip)=.true.
          write(*,'(2f10.4,l5)') min_para(ip), max_para(ip),gaussian(ip)
        enddo
        close(12)






c Initialize RAN3 function

              p=ran3(iseed0)
 





c     DO-LOOP on COMPONENT: 1- R+iT (Modeled); 2- R-iT (Unmodeled)

        do icomp= 1,1

          write(*,*)
          write(*,*)
          write(*,*) ' +++++++++++++++++++ COMPONENT: ', icomp

c       DO-LOOP for each time step

          do isamp=1,n_samp

          if(mod(isamp,100).eq.0)write(*,*) ' -------------- ISAMP: ', isamp



c     ------------------------> Build MaTRICES OBS and H


          if(mod(isamp,100).eq.0)write(*,*) ' +++ BUILD MATRICES OBS and H'

            irow=0
c Radial
            do ibin=1, n_bin
              irow=irow+1
              OBS(irow)=bin(ibin,1,isamp,2)
              STD_OBS(irow)=std_bin(ibin,1,isamp,2)
              H(irow,1)=1.0
              H(irow,2)=cos(deg2rad*baz(ibin))
              H(irow,3)=sin(deg2rad*baz(ibin))
              H(irow,4)=cos(2*deg2rad*baz(ibin))
              H(irow,5)=sin(2*deg2rad*baz(ibin))            
            enddo
c Transverse
            if(icomp.eq.1)shift=90.0
            if(icomp.eq.2)shift=-90.0

            do ibin=1, n_bin
              irow=irow+1
              OBS(irow)=bin(ibin,2,isamp,2)
              STD_OBS(irow)=std_bin(ibin,2,isamp,2)
              H(irow,1)=0.0
              H(irow,2)=cos(deg2rad*(baz(ibin)+ shift ) )
              H(irow,3)=sin(deg2rad*(baz(ibin)+ shift ) )
              H(irow,4)=cos(2*deg2rad*(baz(ibin)+ shift/2.0 ) )
              H(irow,5)=sin(2*deg2rad*(baz(ibin)+ shift/2.0 ) )            
            enddo


c
c
c
c
c HERE THE McMC STARTS (i.e. ther is a different McMC for each time-step)
c
c


          if(mod(isamp,100).eq.0)write(*,*) '  ++++ McMC ----------->      SAMPLING COEFFICENTS '


c
      do imod = 1, ntot

c
c Extract a Candidate model
c
        if(imod.eq.1)then
c
c Starting model is the average/MEAN model
c
	  do ip=1,2*maxharmo+1

            if(.not.gaussian(ip))then
	      para(ip)=0.5*(max_para(ip)+min_para(ip))
            else
              para(ip)=min_para(ip)
            endif

          enddo

        else

	  do ip=1,2*maxharmo+1

            if(.not.gaussian(ip))then

              x0=para0(ip)
              xmin=min_para(ip)
              xmax=max_para(ip)
              sc=delta_para

              call pick_cand_value_uniform(x0,xmin,xmax,sc,x_new)
            
            else
              
              x0=para0(ip)
              xmean=min_para(ip)
              xstd=max_para(ip)
              sc=delta_para

              call pick_cand_value_normal(x0,xmean,xstd,sc,x_new)            

            endif

            para(ip)=x_new

          enddo
        endif

c
c Compute synthetics
c

        do irow = 1, 2*n_bin
          syn(irow)=0.0
          do ip=1,2*maxharmo+1
            syn(irow)=syn(irow)+para(ip)*H(irow,ip)
c            write(96,*)imod, irow, syn(irow), para(ip), H(irow,ip)
          enddo
        enddo

c
c Compute Fit
c

        lppd=0.0
	do irow = 1, 2*n_bin
          lppd=lppd+((obs(irow)-syn(irow))/std_obs(irow))**2
c          write(97,*)imod, irow, obs(irow), syn(irow), lppd
        enddo
c
c
c
c METROPOLIS RULE:
c 
c Select between current and candidate model
c
c

	if(imod.eq.1)then
          lppd0=lppd
          iacc=1
	  do ip=1,2*maxharmo+1
            para0(ip)=para(ip)
          enddo
c          write(98,*) imod, lppd, lppd0, iacc
        else
          if(lppd.lt.lppd0.or..not.posterior)then
            iacc=1
c            write(98,*) imod, lppd, lppd0, iacc
            lppd0=lppd
            iacc=1
            do ip=1,2*maxharmo+1
              para0(ip)=para(ip)
            enddo
          else
            p=ran3(iseed0)
            alpha=exp(-0.5*(lppd-lppd0))
            if(p.lt.alpha)then
              iacc=-1
c              write(98,*) imod, lppd, lppd0, iacc, p, alpha
	      lppd0=lppd
              do ip=1,2*maxharmo+1
                para0(ip)=para(ip)
              enddo
            else
              iacc=0
c              write(98,*) imod, lppd, lppd0, iacc, p, alpha
            endif
          endif
        endif

	if(isamp.eq.301.and.mod(imod,10).eq.0) write(99,'(i8,2f10.2,i4,10f8.4)') imod, lppd, lppd0, iacc, (para0(ip),ip=1,2*maxharmo+1)
        
c
c Collect sample
c   
        do ip=1,2*maxharmo+1
          sample(ip,imod)=para0(ip)
        enddo

      enddo
c end loop on models

c ------------------------------------> END SAMPLING COEFFICENTS



c
c Compute mean and std from collected samples
c

        do ip=1,2*maxharmo+1
          
          mean=0.0
          do imod=mod_burn_in,ntot
            mean=mean+sample(ip,imod)
          enddo
          mean=mean/(ntot-mod_burn_in)

          std=0.0
          do imod=mod_burn_in,ntot
            std=std+(mean-sample(ip,imod))**2
          enddo
          std=sqrt(std/(ntot-mod_burn_in))
        
          C(ip,isamp,icomp)=mean  
          std_C(ip,isamp,icomp)=std
  
        enddo



          enddo
c end loop on samples

        enddo
c end loop on components


        write(*,*)
        write(*,*)


c   WRITE OUTPUT

        do icomp=1,2

          if(icomp.eq.1)pol_c='POS' 
          if(icomp.eq.2)pol_c='NEG'

          do ik=1,2*maxharmo+1
 
            write(k_c,'(i1)')ik
            filename='output/McMC/harm.coeff.'//k_c//'.'//pol_c//'.dat'
            write(*,*) 'WRITE -> ', filename
            open(11,file=filename,status='unknown')
            write(11,*)'# HARMO COEFF'
            do isamp= 1 , n_samp
               write(11,'(i6,2f15.5)')isamp, bin(1,icomp,isamp,1), C(ik,isamp,icomp)
            enddo
            close(11)
            filename='output/McMC/std_harm.coeff.'//k_c//'.'//pol_c//'.dat'
            write(*,*) 'WRITE -> ', filename
            open(11,file=filename,status='unknown')
            write(11,*)'# STD HARMO COEFF'
            do isamp= 1 , n_samp
               write(11,'(i6,2f15.5)')isamp, bin(1,icomp,isamp,1), std_C(ik,isamp,icomp)
            enddo
            close(11)

          enddo

        enddo

        write(*,*)
        write(*,*)


        stop
        end






c
c
c
c
          integer function lofw(word)
c
          character*(*) word
c
          lw=len(word)
          k=0
          do i=1,lw
           if (word(i:i).eq.' ') go to 99
            k=k+1
          end do
 99       lofw=k
c
          return
          end
                                  



c

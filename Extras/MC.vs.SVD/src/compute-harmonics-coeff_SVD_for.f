

       PROGRAM comp_harmo

       implicit none




       integer i, lw, n_bin, lw_dir, lw_name
       integer icomp, isamp, ibin
       integer n_samp, n_header_lines, n_bin_r, n_bin_t
       real*4 phi, shift
       character*128 filename
       character*128 binname
       character*128 bindir
       character*1 comp, k_c
       character*3 phi_c, pol_c, comp2


       integer max_bin, maxsamples, maxharmo, nit_max
       parameter (max_bin=50)
       parameter (maxsamples=1500)
       parameter (maxharmo=2)
       parameter (nit_max=300)
       real*4 bin(max_bin,2,maxsamples,2)
       real*4 harm(0:2,2,maxsamples)
       real*4 std_dev(0:2,2,maxsamples)
       real*4 baz(max_bin), dist(max_bin)

       real*4 deg2rad
       parameter (deg2rad=0.01745329)
       integer iseed0

       real*4 obs(2*max_bin)
       real*4 H(2*max_bin,2*maxharmo+1)
       real*4 H_T(2*maxharmo+1,2*max_bin)
       real*4 HTH(2*maxharmo+1,2*maxharmo+1)
       real*4 HTH_1(2*maxharmo+1,2*maxharmo+1)
       real*4 ident(2*maxharmo+1,2*maxharmo+1)
       real*4 HH(2*maxharmo+1,2*max_bin)
       real*4 CC(2*maxharmo+1)
       real*4 C(2*maxharmo+1,maxsamples,2)
       real*4 C_boot(nit_max,2*maxharmo+1,maxsamples,2)

       real*4 mean, std(maxsamples)

       integer ik, jk, jbin, irow, ib, n_iter, it
       integer ibin_boot(max_bin)
       real*4  p, ran3

       integer lofw


       iseed0=210728

c
c FILE FOR LOG
c
        open(99, file='output/SVD/log.out',status='unknown')

c
        open(13,file='output/SVD/harm.coeff.geom-coverage.dat',status='unknown')
        write(13,*)'# GEOMETRICAL COVERAGE (BAZ and DIST)'

        do icomp=1,2

          if(icomp.eq.1)then
             open(10,file='input/OBS_DATA/list.bin.R.harmo.coeff',status='old')
          endif
          if(icomp.eq.2)then
             open(10,file='input/OBS_DATA/list.bin.T.harmo.coeff',status='old')
          endif

          do ibin = 1, max_bin

           read(10,*,END=101) binname, baz(ibin), dist(ibin)
           write(*,*)' BIN NAME: ', binname, ' from baz/dist: ', baz(ibin),'/', dist(ibin)
           if(icomp.eq.1)write(13,'(2f15.5)') baz(ibin), dist(ibin)
           lw_name=lofw(binname)
c  Read and Load comp of the bin (and its STD) in bin matrix
           filename= 'input/OBS_DATA/'//binname(1:lw_name)
           write(*,*) 'Open: ', filename
           open(11,file=filename, status='old')
           do i=1,1
                read(11,*)
           enddo
           write(*,*) ' reading file ...'
           do isamp= 1, maxsamples
                read(11,*, END=201)bin(ibin,icomp,isamp,1), bin(ibin,icomp,isamp,2)
           enddo
 201       n_samp=isamp-1
           close(11)

          enddo
          
 101      n_bin=ibin-1
          if(icomp.eq.1)n_bin_r=ibin-1
          if(icomp.eq.2)n_bin_t=ibin-1
          close(10)
        enddo
          if(n_bin_t.ne.n_bin_r)
     &       STOP ' - different number of bins in the two list...'

        

        close(13)


c Initialize RAN3 function

              p=ran3(iseed0)
 





c     DO-LOOP for BOOTSTRAP RESAMPLING
c
c     if n_iter equals zero, no bootstrap phase is done and STD is computed from residual
        n_iter=12 
        if(n_iter.gt.nit_max)STOP '- too many bootstrap iterations'
c
        do it=0,n_iter


c Ramdom sampling of bin ensemble

          if(it.ne.0)then
          write(*,*)
          write(*,*) ' ++++++++++++BUILDING POOL FOR IT:: ', it

          do ib=1,n_bin
            p=ran3(iseed0)
            ibin=int(p*n_bin)+1
            if(ibin.lt.1)ibin=1
            if(ibin.gt.n_bin)ibin=n_bin
            ibin_boot(ib)=ibin
            write(*,*) 'Adding bin:', it, ib, ibin
          enddo

          endif
 
c     DO-LOOP on COMPONENT: 1- R+iT (Modeled); 2- R-iT (Unmodeled)

        do icomp= 1,2

          write(*,*)
          write(*,*)
          write(*,*) ' +++++++++++++++++++ COMPONENT: ', icomp

c       DO-LOOP for each time step

          do isamp=1,n_samp

c          write(*,*) ' -------------- ISAMP: ', isamp



c     ------------------------> Build MaTRICES OBS and H

          if(it.eq.0)then

c          write(*,*) ' +++ BUILD MATRICES OBS and H'

            irow=0
c Radial
            do ibin=1, n_bin
              irow=irow+1
              OBS(irow)=bin(ibin,1,isamp,2)
              H(irow,1)=1.0
              H(irow,2)=cos(deg2rad*baz(ibin))
              H(irow,3)=sin(deg2rad*baz(ibin))
              H(irow,4)=cos(2*deg2rad*baz(ibin))
              H(irow,5)=sin(2*deg2rad*baz(ibin))            
            enddo
c Transverse
            if(icomp.eq.1)shift=90.0
            if(icomp.eq.2)shift=-90.0

            do ibin=1, n_bin
              irow=irow+1
              OBS(irow)=bin(ibin,2,isamp,2)
              H(irow,1)=0.0
              H(irow,2)=cos(deg2rad*(baz(ibin)+ shift ) )
              H(irow,3)=sin(deg2rad*(baz(ibin)+ shift ) )
              H(irow,4)=cos(2*deg2rad*(baz(ibin)+ shift/2.0 ) )
              H(irow,5)=sin(2*deg2rad*(baz(ibin)+ shift/2.0 ) )            
            enddo

          else

c            write(*,*) ' >>> BUILD MATRICES OBS and H for ITERATION:', it

            irow=0
c Radial
            do ib=1, n_bin
              ibin=ibin_boot(ib)
              irow=irow+1
              OBS(irow)=bin(ibin,1,isamp,2)
              H(irow,1)=1.0
              H(irow,2)=cos(deg2rad*baz(ibin))
              H(irow,3)=sin(deg2rad*baz(ibin))
              H(irow,4)=cos(2*deg2rad*baz(ibin))
              H(irow,5)=sin(2*deg2rad*baz(ibin))            
            enddo
c Transverse
            if(icomp.eq.1)shift=90.0
            if(icomp.eq.2)shift=-90.0

            do ib=1, n_bin
              ibin=ibin_boot(ib)
              irow=irow+1
              OBS(irow)=bin(ibin,2,isamp,2)
              H(irow,1)=0.0
              H(irow,2)=cos(deg2rad*(baz(ibin)+ shift ) )
              H(irow,3)=sin(deg2rad*(baz(ibin)+ shift ) )
              H(irow,4)=cos(2*deg2rad*(baz(ibin)+ shift/2.0 ) )
              H(irow,5)=sin(2*deg2rad*(baz(ibin)+ shift/2.0 ) )            
            enddo


          endif


c     -------------------------->      STRIPPING COEFFICENTS

c Compute H_T

c          write(*,*) ' +++ COMPUTE H_T'

            do ibin=1,2*n_bin
              do ik=1,2*maxharmo+1
                 H_T(ik,ibin)=H(ibin,ik)
              enddo
            enddo      


c Compute HTH= H_T * H

c          write(*,*) ' +++ COMPUTE HTH'

            do ik=1,2*maxharmo+1
              do jk=1,2*maxharmo+1
                HTH(ik,jk)=0.0
                do ibin=1,2*n_bin
                  HTH(ik,jk) = HTH(ik,jk) + H_T(ik,ibin)*H(ibin,jk)
                enddo
              enddo
            enddo

c Compute HTH_1

c          write(*,*) ' +++ COMPUTE HTH_1'

            ik=2*maxharmo+1
            call compute_inv(ik,HTH,HTH_1,ident)

c Compute HH = HTH_1 * H_T

c          write(*,*) ' +++ COMPUTE HH'

            do ik=1,2*maxharmo+1
              do jbin=1,2*n_bin
                HH(ik,jbin)=0.0
                do jk=1,2*maxharmo+1
                  HH(ik,jbin) = HH(ik,jbin) + HTH_1(ik,jk)*H_T(jk,jbin)
                enddo
              enddo
            enddo

c Compute C = HH * OBS

            do ik=1,2*maxharmo+1
              CC(ik)=0.0
              do jbin=1,2*n_bin
                CC(ik) = CC(ik) + HH(ik,jbin)*OBS(jbin)
              enddo
              if(it.eq.0)C(ik,isamp, icomp)=CC(ik)
              if(it.ne.0)C_boot(it,ik,isamp,icomp)=CC(ik)
            enddo

c            write(*,*) ' -> C: ',(C(ik,isamp,icomp),ik=1,2*maxharmo+1)



          enddo
c end loop on samples

        enddo
c end loop on components

        enddo
c end loop on iterations for bootstrap resampling




        if(n_iter.eq.0)then

c     -------------------------->      COMPUTING CLASSICAL STD of K=0 from residual

c       DO-LOOP for each time step

          do isamp=1,n_samp

          std(isamp)=0.0

          do ibin=1,n_bin

             mean= C(1,isamp,1) +
     &             C(2,isamp,1) * cos(deg2rad*baz(ibin)) +
     &             C(3,isamp,1) * sin(deg2rad*baz(ibin)) +
     &             C(4,isamp,1) * cos(2*deg2rad*baz(ibin)) +
     &             C(5,isamp,1) * sin(2*deg2rad*baz(ibin)) 

             std(isamp) = std(isamp) + (mean - bin(ibin,1,isamp,2))**2

          enddo
          
          std(isamp)= sqrt( std(isamp)/float(n_bin-1) )

          enddo
c end loop on samples

          else

c     -------------------------->      COMPUTING STD of K=0 from BOOTSTRAP RESAMPLING

c       DO-LOOP for each time step

          do isamp=1,n_samp

            mean= C(1,isamp,1)
            std(isamp)=0.0

          do it=1,n_iter

             std(isamp) = std(isamp) + (mean - C_boot(it,1,isamp,1))**2

          enddo
          
          std(isamp)= sqrt( std(isamp)/float(n_iter-1) )

          

          enddo
c end loop on samples

          endif
c end if-loop for STD computation





c   WRITE OUTPUT

        do icomp=1,2

          if(icomp.eq.1)pol_c='POS' 
          if(icomp.eq.2)pol_c='NEG'

          do ik=1,2*maxharmo+1
 
            write(k_c,'(i1)')ik
            filename='output/SVD/harm.coeff.'//k_c//'.'//pol_c//'.dat'
            write(*,*) 'WRITE -> ', filename
            open(11,file=filename,status='unknown')
            write(11,*)'# HARMO COEFF'
            do isamp= 1 , n_samp
               write(11,'(i6,2f15.5)')isamp, bin(1,icomp,isamp,1), C(ik,isamp,icomp)
            enddo
            close(11)

          enddo

        enddo

        filename='output/SVD/std_harm.coeff.1.POS.dat'
        write(*,*) 'WRITE -> ', filename
        open(11,file=filename,status='unknown')
        write(11,*)'# STD HARMO COEFF'
        do isamp= 1 , n_samp
          write(11,'(i6,2f15.5)')isamp, bin(1,1,isamp,1), std(isamp)
        enddo
        close(11)
            



        stop
        end






c
c
c
c
          integer function lofw(word)
c
          character*(*) word
c
          lw=len(word)
          k=0
          do i=1,lw
           if (word(i:i).eq.' ') go to 99
            k=k+1
          end do
 99       lofw=k
c
          return
          end
                                  



c

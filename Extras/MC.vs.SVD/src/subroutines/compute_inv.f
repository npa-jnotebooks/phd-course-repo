
      subroutine  compute_inv(n,cov,icov,ident)

        implicit none
        integer max_n
        parameter (max_n=5)

        integer max_n_eigenvalues
        integer max_n_diag     
        real*4  min_covar, min_eigenvalue
        parameter (max_n_eigenvalues=100)
        parameter (max_n_diag=10)
        parameter (min_covar=0.001)
        parameter (min_eigenvalue=0.001)


        integer n, i, j, k
        integer n_eigenvalues

        real*4  icov(max_n,max_n)
        real*4  cov(max_n,max_n)
        real*4  ident(max_n,max_n)
        real*4  a(max_n,max_n)
        real*4  b(max_n,max_n)
        real*4  u(max_n,max_n)
        real*4  v(max_n,max_n)
        real*4  v_T(max_n,max_n)
        real*4  w(max_n)
        real*4  q


c$$$c TEST --> Moho: 18km; VpVs: 1.87
c$$$
c$$$        cov(1,1)=0.46026504
c$$$        cov(1,2)=8.54911232
c$$$        cov(2,1)=8.54911232
c$$$        cov(2,2)=176.22963
c$$$c TEST 
c$$$        cov(1,1)=3
c$$$        cov(1,2)=0
c$$$        cov(2,1)=0
c$$$        cov(2,2)=2


        do i=1,n
          do j=1,n
             a(i,j)= cov(i,j)
             icov(i,j)=0.0
             ident(i,j)=0.0
          enddo
        enddo



        call svdcmp(a,n,n,max_n,max_n,w,v)


        n_eigenvalues=0
        do i=1,n
           if(abs(w(i)).lt.min_eigenvalue) then
              w(i)=0.0
           else
              n_eigenvalues = n_eigenvalues + 1
           endif
        enddo

        if (n_eigenvalues.gt.max_n_eigenvalues) goto 201

        do i=1,n
          do j=1,n
             u(i,j)=a(i,j)
             v_T(i,j)=v(j,i)
             if(w(j).gt.0.0)b(i,j)=u(i,j)*(1/w(j))
          enddo
        enddo        

        do i=1,n
          do j=1,n
             q = 0.0
            do k=1,n
               q = q + b(i,k)*v_T(k,j)
            enddo
            icov(i,j) = q
          enddo
        enddo

c$$$        write(*,*) 'COV matrix: '
c$$$        do i=1,n
c$$$          write(*,*) (cov(i,j),j=1,n)
c$$$        enddo
c$$$                             
c$$$        write(*,*) '-> Eigenv: ', (w(j),j=1,n) 
c$$$
c$$$        write(*,*) 'ICOV matrix: '
c$$$        do i=1,n
c$$$          write(*,*) (icov(i,j),j=1,n)
c$$$        enddo
c$$$        
        do i=1,n
          do j=1,n
            ident(i,j)=0.0
            do k= 1,n
              ident(i,j)=ident(i,j) + 
     &                         (icov(i,k)*cov(k,j))
            enddo
          enddo
        enddo

c$$$        write(*,*) ' IDENTITY matrix '
c$$$        do i=1,n
c$$$          write(*,*) (ident(i,j),j=1,n)
c$$$        enddo
c$$$
                

        return

 201    write(*,*) ' *** ERROR - number of eigenvalues: ', n_eigenvalues
        write(*,*) '    in SVD decomposition of COVARIANCE matrix '
        write(*,*) '       (out of ,',max_n_eigenvalues,' max)' 
        stop

        end

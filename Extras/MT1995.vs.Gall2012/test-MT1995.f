


	PROGRAM TEST_MT1995


	implicit none

        include 'common.randomtype'

	real*4  prior(2), like(2), post(2), prop(2), evidence
        real*4 like_ratio, alpha(2)
	integer it, itmax, cur, cand, cur_tmp

	real*4  p, ran3


        open(98,file='distrib_MT95.dat',status='unknown')
        open(99,file='distrib_G2012.dat',status='unknown')



        prior(1)=0.5
        prior(2)=0.5
        like(1)=0.8
        like(2)=0.2

        evidence=prior(1)*like(1)+prior(2)*like(2)
        post(1)=prior(1)*like(1)/evidence
        post(2)=prior(2)*like(2)/evidence

        prop(1)=0.5
        prop(2)=0.5

        alpha(1)=(prop(2)/prop(1)) * (post(1)/post(2))
	alpha(2)=(prop(1)/prop(2)) * (post(2)/post(1))

        itmax=100000

c Start from M1

        cur=1
        cand=2

	do it= 1, itmax

c MT1995
          p=ran3(iseed0)
          if(p.lt.prior(cand))then

            like_ratio=like(cand)/like(cur)
            p=ran3(iseed0)
            
            if(p.lt.like_ratio)then
              cur_tmp=cur
              cur=cand
              cand=cur_tmp
            endif

          endif

          write(98,201) 'MT1995:', it, cur
 

c GALL2012

          p=ran3(iseed0)
          if(p.lt.alpha(cand))then
            cur_tmp=cur
            cur=cand
            cand=cur_tmp
          endif

          write(99,201) 'G2012:', it, cur

        enddo

 201   format(a,2i10)

       close(99)
       close(98)


       stop
       end





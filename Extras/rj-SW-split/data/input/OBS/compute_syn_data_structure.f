



	PROGRAM SYNT_DATA_STRUCTURE

	implicit none

	integer id, ip, ic
        character*6 id_lab
        character*7 cdummy
	real*4 data(2000,5), iseed0, ran3, p, day, zdummy

        iseed0=212121

        open(10,file='data.obs.ORIGINAL_from2011',status='old')
        read(10,*)
        read(10,*)
        read(10,*)
        read(10,*)
        read(10,*)
        do id=1,1100
          read(10,*) cdummy, zdummy, zdummy, zdummy, data(id,4), data(id,5)
        enddo


	do id=5,1004

            write(id_lab,'(i6)')id-4
            do ic=1,6
              if(id_lab(ic:ic).eq.' ')id_lab(ic:ic)='0'
            enddo
            p=ran3(iseed0)
	    day=0.5*( (1.0*id-3.0)+p*6.0 ) *0.99

         
       	   write(*,'(a,a6,5f12.3)') '#',id_lab, day, 1.0, 1.0, data(id,4), data(id,5)
 
         enddo

         stop
         end
         
         
c
c ----------------------------------------------------------------------------
c						
c	Numerical Recipes random number generator
c
c ----------------------------------------------------------------------------
      FUNCTION ran3(idum)
      INTEGER idum
      INTEGER MBIG,MSEED,MZ
C     REAL MBIG,MSEED,MZ
      REAL*4 ran3,FAC
      PARAMETER (MBIG=1000000000,MSEED=161803398,MZ=0,FAC=1./MBIG)
C     PARAMETER (MBIG=4000000.,MSEED=1618033.,MZ=0.,FAC=1./MBIG)
      INTEGER i,iff,ii,inext,inextp,k
      INTEGER mj,mk,ma(55)
C     REAL mj,mk,ma(55)
      SAVE iff,inext,inextp,ma
      DATA iff /0/
      if(idum.lt.0.or.iff.eq.0)then
        iff=1
        mj=MSEED-iabs(idum)
        mj=mod(mj,MBIG)
        ma(55)=mj
        mk=1
        do 11 i=1,54
          ii=mod(21*i,55)
          ma(ii)=mk
          mk=mj-mk
          if(mk.lt.MZ)mk=mk+MBIG
          mj=ma(ii)
11      continue
        do 13 k=1,4
          do 12 i=1,55
            ma(i)=ma(i)-ma(1+mod(i+30,55))
            if(ma(i).lt.MZ)ma(i)=ma(i)+MBIG
12        continue
13      continue
        inext=0
        inextp=31
        idum=1
      endif
      inext=inext+1
      if(inext.eq.56)inext=1
      inextp=inextp+1
      if(inextp.eq.56)inextp=1
      mj=ma(inext)-ma(inextp)
      if(mj.lt.MZ)mj=mj+MBIG
      ma(inext)=mj
      ran3=mj*FAC
c
      if(ran3.gt.1.0)ran3=ran3-1.0
      if(ran3.lt.0.0)ran3=ran3+1.0
c
      return
      END
c
c ----------------------------------------------------------------------------
c

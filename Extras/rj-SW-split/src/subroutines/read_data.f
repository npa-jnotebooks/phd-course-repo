

        subroutine read_data


c Read data file: input/OBS/data.obs and store value in COMMON BLOCK

        include '../trans_dim.para'


        include '../common.obs_syn'
        include '../common.param'

c
c Scratch Variables 

        integer  id
        character*7 cdum


        open(12,file='input/OBS/data.obs',status='old')

        read(12,*)
        read(12,*)
c Read T MIN and T MAX
        read(12,*)t_min, t_max
c Read NUMBER OF DATA
        read(12,*) ndata

        read(12,*) 
        do id=1,ndata
          read(12,*) cdum, obs_D(id,1),obs_D(id,2),obs_D(id,3),obs_D(id,4), obs_D(id,5)
        enddo

c
c -----------------------------------------------------------------------
c WRITE OUT OBSERVED DATA for REFERENCE
c
        open(13,file='output/obs.data',status='unknown')

        do id=1,ndata
          write(13,101) 'OBS DATA:', id, obs_D(id,1),obs_D(id,2),obs_D(id,3),obs_D(id,4), obs_D(id,5)
        enddo

 101    format(a,i6,5f16.4)
c
c -----------------------------------------------------------------------
c


        close(12)


        return
        end



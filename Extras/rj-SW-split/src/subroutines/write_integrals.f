c
c ----------------------------------------------------------------------------
c
      subroutine write_integrals
c
c ----------------------------------------------------------------------------
c
c  Input/output: (none) -- all quantities are read from common blocks
c
c ----------------------------------------------------------------------------
c


      include '../trans_dim.para'
      include '../common.integral'
      include '../common.param'
      include '../common.obs_syn'
      include '../common.recipe'


c Scratch variables
      integer iq, ic, id, idiv, ii, ix
      integer tot_mod
      real*4 v(max_n_grid), cur, freq
      real*4 smean, std(max_n_grid)
      real*4 min_p, max_p, mean, diff
      real*4 delta_x
      real*4 x(max_n_grid)
      character*3 q_char, id_char
      character filename3*(namelen)
c
c
c

      write(*,*)
      write(*,*) '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
      write(*,*) '   OUTPUT OF INTEGRALS of DT and THETA'
      write(*,*) '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
      write(*,*)


c
c  HYPER-PARAMETERS
c


         write(q_char,'(i3)') 999
         do ic=1,3
            if(q_char(ic:ic).eq.' ') q_char(ic:ic)='0'
         enddo

      do id=1,2

         write(id_char,'(i3)') id
         do ic=1,3
            if(id_char(ic:ic).eq.' ') id_char(ic:ic)='0'
         enddo

         filename3='output/integral'//q_char//'.'//id_char//'.out'
c         write(*,*) ' Writing file: ',filename3,' ...'
         open(iounit1,file=filename3,status='unknown')
         write(iounit1,'(2i4)')999, id
         write(iounit1,'(2f12.5)') bound_hparam(id,1), bound_hparam(id,2)
         do idiv=1, ndiv
            write(iounit1,'(i3,i15)')idiv,int_hparam(id,idiv)
         enddo
         close(iounit1)

      enddo




c
c
c  number of complexities for each quality
c  
 
         iq=1
 
         write(q_char,'(i3)') iq
         do ic=1,3
            if(q_char(ic:ic).eq.' ') q_char(ic:ic)='0'
         enddo
         id_char='000'
         filename3='output/integral'//q_char//'.'//id_char//'.out'
c         write(*,*) ' Writing file: ',filename3,' ...'
         open(iounit1,file=filename3,status='unknown')
         write(iounit1,'(2i4)')iq, 0
         write(iounit1,'(2i5)') bound_nc(1), bound_nc(2)
         do idiv=bound_nc(1), bound_nc(2)
            write(iounit1,'(i6,i15)')idiv,int_nc(idiv)
         enddo
         close(iounit1)


c
c
c
c  Boundary b/w Voronoi cells
c

         iq=1

         write(q_char,'(i3)') iq
         do ic=1,3
            if(q_char(ic:ic).eq.' ') q_char(ic:ic)='0'
         enddo
         id_char='999'
         filename3='output/integral'//q_char//'.'//id_char//'.out'
c         write(*,*) ' Writing file: ',filename3,' ...'
         open(iounit1,file=filename3,status='unknown')
         write(iounit1,'(2i4)')iq, 999
         write(iounit1,'(2i5)') bound_pos(1), bound_pos(2)
         do idiv=1, nstep_map
            write(iounit1,'(i6,i15)')idiv,int_pos(idiv)
         enddo
         close(iounit1)







c
c MEAN and STD of the DT, THETA and Density
c

      delta_x=(t_max-t_min)/(nstep_map-1)

      iq=1

         write(q_char,'(i3)') iq
         do ic=1,3
            if(q_char(ic:ic).eq.' ') q_char(ic:ic)='0'
         enddo

      do id=1,3

         write(id_char,'(i3)') id
         do ic=1,3
            if(id_char(ic:ic).eq.' ') id_char(ic:ic)='0'
         enddo

c COMPUTE AVERAGE MODELS

         do ix=1,nstep_map

           x(ix)=t_min+(ix-1)*delta_x

           mean=0.0
           tot_mod=0
           min_p=bound_grid(ix,id,1)
           max_p=bound_grid(ix,id,2)

           do ii=1, ndiv
             cur=min_p+(1.d0*ii-0.5d0)/(1.d0*ndiv)*(max_p-min_p)
             mean=mean+1.d0*int_grid(ix,id,ii)*cur
             tot_mod=tot_mod+int_grid(ix,id,ii)
           enddo
           mean=mean/(1.d0*tot_mod)


           v(ix)=mean


           smean=0.0
           min_p=bound_grid(ix,id,1)
           max_p=bound_grid(ix,id,2)

           do ii=1, ndiv
             diff=v(ix)-( min_p+(1.d0*ii-0.5d0)/(1.d0*ndiv)*(max_p-min_p) )
             smean = smean +
     &               1.d0 * int_grid(ix,id,ii) * (diff**2)
           enddo

           std(ix)=1.d0*sqrt(smean/(1.d0*tot_mod))


         enddo

c WRITE MODELS

         filename3=
     &   'output/mean'//q_char//'.'//id_char//'.out'
c         write(*,*) ' Writing file: ',filename3,' ...'
         open(iounit1,file=filename3,status='unknown')
         write(iounit1,'(3i4)')iq, id
         do ix=1, nstep_map
           write(iounit1,'(4f10.4)' ) x(ix), v(ix), std(ix)
         enddo
        
         close(iounit1)

      enddo






c
c WRITE FULL PPD
c

      delta_x=(t_max-t_min)/(nstep_map-1)

      iq=1

         write(q_char,'(i3)') iq
         do ic=1,3
            if(q_char(ic:ic).eq.' ') q_char(ic:ic)='0'
         enddo

      do id=1,2

         write(id_char,'(i3)') id
         do ic=1,3
            if(id_char(ic:ic).eq.' ') id_char(ic:ic)='0'
         enddo

         filename3=
     &   'output/integral'//q_char//'.'//id_char//'.out'
c         write(*,*) ' Writing file: ',filename3,' ...'
         open(iounit1,file=filename3,status='unknown')
         write(iounit1,'(3i4)')iq, id

         tot_mod=0
         do ii=1, ndiv
           tot_mod=tot_mod+int_grid(1,id,ii)
         enddo


         do ix=1,nstep_map

           x(ix)=t_min+(ix-1)*delta_x

           min_p=bound_grid(ix,id,1)
           max_p=bound_grid(ix,id,2)

           do ii=1, ndiv
             cur=min_p+(1.d0*ii-0.5d0)/(1.d0*ndiv)*(max_p-min_p)
             freq=(1.d0*int_grid(ix,id,ii))/(1.0*tot_mod)
             write(iounit1,'(3f10.4)' ) x(ix), cur, freq
           enddo

         enddo

         close(iounit1)

      enddo

      return
      end

c

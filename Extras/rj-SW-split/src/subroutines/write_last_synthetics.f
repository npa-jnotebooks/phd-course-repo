c
c ----------------------------------------------------------------------------
c
      subroutine write_last_synthetics(hparam)
c
c ----------------------------------------------------------------------------
c


      include '../trans_dim.para'
      include '../common.obs_syn'
      include '../common.obs_syn2'
      include '../common.recipe'

c Main variables

      real*4  hparam(nhparam_max)


c Scratch variables

      integer id, ic, iadd
      real*4 mean1, std1, mean2, std2, std, x
      character filename3*(namelen)
      character*6 id_lab
c
c
c

       write(*,*)
       write(*,*) '----------------------------------------------'
       write(*,*) ' WRITE SYNTHETICS from the LAST sampled model '
       write(*,*) '----------------------------------------------'
       write(*,*)

	write(*,*)
        write(*,*) ' WARNING:: Synthetics are created using white noise. '
        write(*,*) ' WARNING:: Standard deviation in file data.obs are   '
        write(*,*) ' WARNING:: scaled using the hparam(1) and hparam(2). '
        write(*,*) 
c
c
c
c
c
c
         filename3='output/syn.dat'
         write(*,*) ' Writing file: ',filename3,' ...'
         open(iounit1,file=filename3,status='unknown')
         write(iounit1,'(a)') '# SYNTHETICS for DT and THETA '
         iadd=0
         do id=1, ndata
            write(id_lab,'(i6)')id
            do ic=1,6
              if(id_lab(ic:ic).eq.' ')id_lab(ic:ic)='0'
            enddo
            mean1=syn_D0(id,1)
            std1=obs_D(id,4)
            mean2=syn_D0(id,2)
            std2=obs_D(id,5)

            if(added_noise)then
              std=std1*(10**(hparam(1)))
              call sampleNormal(mean1,std,x)
              if(x.lt.0.0)then
                iadd=iadd+1
                x=0.0
              endif
              mean1=x
              std=std2*(10**(hparam(2)))
              call sampleNormal(mean2,std,x)
              if(x.lt.0.0)then
                iadd=iadd+1
                x=0.0
              endif
              if(x.gt.180.0)then
                x=180.0
                iadd=iadd+1
              endif
              mean2=x
            endif

            write(iounit1,'(a,a6,5f12.3)')'#',id_lab, obs_D(id,1), mean1, mean2, std1, std2 
         enddo
         close(iounit1)

         if(iadd.gt.0)then
           write(*,*)
           write(*,'(a,i6)') ' --> DATA MODIFIED FOR BOUND COND VIOLATION: ', iadd
           write(*,*)
         endif





      return
      end

c

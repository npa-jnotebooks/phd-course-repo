


	PROGRAM TEST_UNIFORM


	implicit none

        include 'common.randomtype'

	real*4  x, xmin, xmax, sc, min_dist
        real*4  param(100), param_unsorted(100), param_sorted(100), changepoint(100)
        real*4  min_prob, max_prob, delta_prob
        real*4  dist, x0
	integer ic, it1, it, n, ic_sele, itmax
        logical too_close

	real*4  p, ran3


        open(98,file='x_change.dat',status='unknown')
        open(99,file='x_vnuc.dat',status='unknown')



        iseed0=210827
        min_dist=2.0
	xmin=0.0
	xmax=500.0
        sc=0.05
        n=10
        itmax=100000

c Initialize Array param0()

	do ic= 1, n

          p=ran3(iseed0)
          write(*,*) 'STARTING P:' , p
          param(ic)=xmin+p*(xmax-xmin)

        enddo


c Iterate

        do it=1,itmax

c Select Vnuc

          
          ic_sele=-1
          min_prob=0.0
          max_prob=0.0
          delta_prob=1.0/(1.0*n)

          p=ran3(iseed0)
          do ic = 1, n
            min_prob=max_prob
            max_prob=min_prob+delta_prob
            if(min_prob.lt.p.and.p.le.max_prob)ic_sele=ic 
          enddo
          if(ic_sele.lt.1)ic_sele=n

          if(it.eq.1)write(*,*) 'FIRST_NUC_SELE:', ic_sele

c Set new param         
            
          do it1 = 1, 1000

            x0=param(ic_sele)
            call pick_cand_value_uniform(x0,xmin,xmax,sc,x)

c Check min dist between Vnuc

            too_close=.false.
            dist=1.e10
            do ic=1,n
              if(ic.ne.ic_sele)then
                dist=abs(param(ic)-x)
                if(dist.lt.min_dist)too_close=.true.
              endif
            enddo

            if(.not.too_close)then
              param(ic_sele)=x
              goto 203
            else
              write(*,*) ' Testing new pos:', ic_sele, param(ic_sele), x, it1
            endif

         enddo

 203     continue

c Compute changepoints position

         do ic = 1, n
           param_unsorted(ic)=param(ic)
         enddo

         call sort(param_unsorted,n,param_sorted)

         do ic = 2, n
           changepoint(ic-1)=0.5*(param_sorted(ic)+param_sorted(ic-1))
         enddo

c Write out new positions

         do ic = 1, n
           write(99,201) 'VNUC:', it, ic, param(ic)
         enddo
         do ic = 1, n-1
           write(98,202) 'CHAN:', it, ic, changepoint(ic)
         enddo

 201     format(a,2i10,f10.2)
 202     format(a,2i10,f10.2)



       enddo


       close(99)
       close(98)


       stop
       end





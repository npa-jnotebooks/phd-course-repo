c
c
c	subroutine FORWARD
c
c--------------------------------------------------------------------
c
c INPUT:
c         n --   number of Voronoi nuclei
c         param -- parameter values for each Voronoi nucleus
c
c OUTPUT:
c         (none) -- syn_data is stored in a COMMON BLOCK
c
c--------------------------------------------------------------------
c
c
        subroutine forward(n,param)


        include '../trans_dim.para'


        include '../modules/common.obs_syn'
        include '../modules/common.param'
        include '../modules/common.recipe'


c
c MAIN variables

        integer n
        real*4  param(nc_max,nd_max)



c
c Scratch variables
 
        integer ic, id, ic_nearest
        real*4  dist, dist_min


          if(info)then
          write(*,'(a)') ' ++RjSurfRec:: MODEL for FORWARD: '
          do ic=1,n
            write(*,'(a,i4,3f16.4)') ' ++RjSurfRec:: NUCLEUS: ',ic,(param(ic,id), id=1,nd)
          enddo
          write(*,*)
          endif




        do id=1,ndata

c Find the "nearest neighbour" of id-th data

           ic_nearest=-1
           dist_min=10e20

           do ic=1,n

             dist=sqrt( (param(ic,1)-obs_D(id,1))**2 + (param(ic,2)-obs_D(id,2))**2 )

             if(dist.lt.dist_min)then
               dist_min=dist
               ic_nearest=ic
             endif
         
           enddo


           syn_D(id)=param(ic_nearest,3)

c
c END DO-LOOP over NDATA
c
        enddo

        if(info)then
          write(*,*)
          write(*,'(a)') ' ++RjSurfRec::  FORWARD:'
          write(*,'(a)') ' ++RjSurfRec::  Obs_D and syn_D (first 20 values) '
          write(*,'(a,20f8.3)') 'DT-SYN:',(syn_D(id), id=1,20)
          write(*,'(a,20f8.3)') 'DT-OBS:',(obs_D(id,3), id=1,20)
          write(*,*)
        endif


        return
        end

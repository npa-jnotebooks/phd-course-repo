c
c ----------------------------------------------------------------------------
c
      subroutine write_integrals
c
c ----------------------------------------------------------------------------
c
c  Input/output: (none) -- all quantities are read from common blocks
c
c ----------------------------------------------------------------------------
c


      include '../trans_dim.para'
      include '../modules/common.integral'
      include '../modules/common.param'
      include '../modules/common.obs_syn'
      include '../modules/common.recipe'


c Scratch variables
      integer iq, ic, id, ii, ix, iy
      integer tot_mod
      integer start_nx, end_nx
      integer start_ny, end_ny
      real*4  acc(max_n_grid,max_n_grid), cur, freq
      real*4  smean, std(max_n_grid,max_n_grid)
      real*4  min_p, max_p, mean, diff
      real*4  delta_x, delta_y
      real*4  x(max_n_grid), y(max_n_grid)
      character*3 q_char, id_char
      character filename3*(namelen)
      character filename4*(namelen)
c
c
c

      write(*,*)
      write(*,*) '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
      write(*,*) ' OUTPUT OF INTEGRALS of MOHO DEPTH and Density'
      write(*,*) '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
      write(*,*)



c
c MEAN and STD of the ACC 
c

      delta_x=(x_max-x_min)/(nstep_map-1)
      delta_y=(y_max-y_min)/(nstep_map-1)

      iq=1

         write(q_char,'(i3)') iq
         do ic=1,3
            if(q_char(ic:ic).eq.' ') q_char(ic:ic)='0'
         enddo

      do id=1,1

         write(id_char,'(i3)') id
         do ic=1,3
            if(id_char(ic:ic).eq.' ') id_char(ic:ic)='0'
         enddo

         do ix=1,nstep_map
         do iy=1,nstep_map

           x(ix)=x_min+(ix-1)*delta_x
           y(iy)=y_min+(iy-1)*delta_y

           mean=0.0
           tot_mod=0
           min_p=bound_grid(ix,iy,id,1)
           max_p=bound_grid(ix,iy,id,2)

           do ii=1, ndiv
             cur=min_p+(1.d0*ii-0.5d0)/(1.d0*ndiv)*(max_p-min_p)
             mean=mean+1.d0*int_grid(ix,iy,id,ii)*cur
             tot_mod=tot_mod+int_grid(ix,iy,id,ii)
           enddo
           mean=mean/(1.d0*tot_mod)


           acc(ix,iy)=mean
	   

           smean=0.0
           min_p=bound_grid(ix,iy,id,1)
           max_p=bound_grid(ix,iy,id,2)

           do ii=1, ndiv
             diff=acc(ix,iy)-( min_p+(1.d0*ii-0.5d0)/(1.d0*ndiv)*(max_p-min_p) )
             smean = smean +
     &               1.d0 * int_grid(ix,iy,id,ii) * (diff**2)
           enddo

           std(ix,iy)=1.d0*sqrt(smean/(1.d0*tot_mod))



         enddo
         enddo


c WRITE MODELS

         filename3=
     &   'output/mean'//q_char//'.'//id_char//'.out'
c         write(*,*) ' Writing file: ',filename3,' ...'
         open(iounit1,file=filename3,status='unknown')
	 filename4='output/mean001.002.out'
         open(iounit2,file=filename4,status='unknown')
         write(iounit1,'(3i4)')iq, id
         write(iounit2,'(3i4)')iq, 2
         do ix=1, nstep_map
         do iy=1, nstep_map
           write(iounit1,'(4f10.4)' ) x(ix), y(iy), acc(ix,iy), std(ix,iy)
           write(iounit2,'(2f10.4,i9)' ) x(ix), y(iy), int_grid_density(ix,iy)
         enddo
         enddo
        
         close(iounit1)

      enddo






c
c WRITE FULL PPD FOR SELECTED SPOTS
c
c Selectected spot will have have: 
c
      start_nx=50
      end_nx=51
      start_ny=50
      end_ny=51
c

      delta_x=(x_max-x_min)/(nstep_map-1)
      delta_y=(y_max-y_min)/(nstep_map-1)

      iq=1

         write(q_char,'(i3)') iq
         do ic=1,3
            if(q_char(ic:ic).eq.' ') q_char(ic:ic)='0'
         enddo

      do id=1,1

         write(id_char,'(i3)') id
         do ic=1,3
            if(id_char(ic:ic).eq.' ') id_char(ic:ic)='0'
         enddo

         filename3=
     &   'output/integral'//q_char//'.'//id_char//'.out'
c         write(*,*) ' Writing file: ',filename3,' ...'
         open(iounit1,file=filename3,status='unknown')
         write(iounit1,'(3i4)')iq, id

         tot_mod=0
         do ii=1, ndiv
           tot_mod=tot_mod+int_grid(1,1,id,ii)
         enddo


         do ix=start_nx,end_nx
         do iy=start_ny,end_ny

           x(ix)=x_min+(ix-1)*delta_x
           y(iy)=y_min+(iy-1)*delta_y

           min_p=bound_grid(ix,iy,id,1)
           max_p=bound_grid(ix,iy,id,2)

           do ii=1, ndiv
             cur=min_p+(1.d0*ii-0.5d0)/(1.d0*ndiv)*(max_p-min_p)
             freq=(1.d0*int_grid(ix,iy,id,ii))/(1.0*tot_mod)
             write(iounit1,'(4f10.4)' ) x(ix), y(iy), cur, freq
           enddo

         enddo
         enddo

         close(iounit1)

      enddo




	return
	end
